<?php

/**
 * Created by PhpStorm.
 * Auther @ Mads Roloff - Rights reservede to Author
 * Date: 19-06-2017
 */
class contact
{
    private $db;
    public $iContactID = -1;
    public $vcImage;
    public $vcTitle;
    public $vcShortDesc;
    public $txLongDesc;

    public $daExpireDate;
    public $daStartDate;
    public $vcPlace;
    public $iDonate;
    public $vcDate;
    public $daCreated;
    public $iSuspended;
    public $iDeleted;
    Public $vcImagePath;

    public $arrFormElms = array();
    public $arrLabels = array();
    public $arrColumns = array();
    public $arrValues = array();
    public $unset = array();

    public function __construct()
    {
        global $db;
        $this->db = $db;
        //$this->CreateTable();
        $this->Table = "contact";

        $this->arrFormElm = array(
            "iContactID" => array("label" => "", "type" => "hidden", "placeholder" => "", "required" => "data-requried='1'", "dbname" => "iContactID", "Filter" => FILTER_SANITIZE_NUMBER_INT),
            "vcTitle" => array("label" => "Contact Titel", "type" => "text", "placeholder" => "Contact Titel", "required" => "data-requried='1'", "dbname" => "vcTitle", "Filter" => FILTER_SANITIZE_STRING),
            "vcAddress" => array("label" => "Contact Titel", "type" => "text", "placeholder" => "Contact Titel", "required" => "data-requried='1'", "dbname" => "vcAddress", "Filter" => FILTER_SANITIZE_STRING),
            "iPhoneNumber" => array("label" => "Contact Titel", "type" => "text", "placeholder" => "Contact Titel", "required" => "data-requried='1'", "dbname" => "iPhoneNumber", "Filter" => FILTER_SANITIZE_NUMBER_INT),
            "txShortDesc" => array("label" => "Contact Titel", "type" => "textarea", "placeholder" => "Contact Titel", "required" => "data-requried='1'", "dbname" => "txShortDesc", "Filter" => FILTER_SANITIZE_STRING),


        );

    }

    public function GetSelect()
    {
        $sql = "SELECT * FROM $this->Table WHERE iDeleted = 0";
        return $this->db->_fetch_array($sql, array());
    }

    public function GetList()
    {
        $sql = "SELECT * FROM $this->Table WHERE iDeleted = 0";
        //Shows Column names from "Contact"
        $sqlLabels = "SHOW FULL COLUMNS FROM $this->Table";
        $this->arrLabels = $this->db->_fetch_array($sqlLabels, array());
        $this->arrValues = $this->db->_fetch_array($sql, array());

        //UNSET = Columns to avoid in LIST
        $this->unset = array(
            "iContactID",
            "daCreated",
            "iSuspended",
            "iDeleted",

        );
    }

    public function getDetails($id)
    {
        $sql = "SELECT * FROM $this->Table WHERE iDeleted = 0 AND iContactID = ?";
        //Shows Column names from "$this->table"
        $sqlLabels = "SHOW FULL COLUMNS FROM $this->Table";
        $this->arrLabels = $this->db->_fetch_array($sqlLabels, array());
        $this->arrValues = $this->db->_fetch_array($sql, array($id));

        //UNSET = Columns to avoid in LIST EDIT FOR DIFFRENT USE!
        $this->unset = array(
            "iContactID",
            "daCreated",
            "iSuspended",
            "iDeleted",
        );
    }

    /**
     * Get Contact
     * change $this->"class for use"
     * Used for edit mode
     */
    public function getContact($id)
    {
        $this->iContactID = $id;
        $sql = "SELECT * FROM $this->Table WHERE iDeleted = 0 AND $id = ?";
        $row = $this->db->_fetch_array($sql, array($id));
        foreach ($row as $key => $value) {
            $this->$key = $value;
        }
        return $row;
    }

    public function FooterContact($limit)
    {
        $sql = "SELECT * FROM $this->Table WHERE iDeleted = 0 ORDER BY daCreated DESC LIMIT $limit";
        $row = $this->db->_fetch_array($sql, array());
        foreach ($row[0] as $key => $value) {
            $this->$key = $value;
        }
    }

    public function UpdateContact($iContactID)
    {
        //unsets IContactID from the array
        unset($this->arrFormElm["iContactID"]);

        //MAkes a foreach that gets "filter" from $arrFormElm
        foreach ($this->arrFormElm as $key => $value) {
            $f[$key] = filter_input(INPUT_POST, $key, $value["Filter"]);
        }
        //uses the value of the "filter"'s and takes the value from the $_POST and inserts them into an array ordered by the "FIlTER"
        $params = array_values($f);

        //iContactID = IContactID
        $this->iContactID = $iContactID;

        //awesome SQL that makes an string that looks like this
        //"UPDATE Contact SET vcTitle = ?, vcLastName = ?, vcContactName = ?, vcPassword = ?, vcAddress = ?, iZip = ?, vcCity = ?, vcEmail = ?, vcPhone1 = ?, vcPhone2 = ? WHERE iContactID = 12"
        echo $sql = "UPDATE $this->Table SET " . implode(array_keys($this->arrFormElm), " = ?, ") . " = ? WHERE iContactID = $this->iContactID";
        return $this->db->_query($sql, $params);
    }

    public function DeleteContact($iContactID)
    {
        $this->iContactID = $iContactID;
        //SQL that sets iDeleted to 1
        $sql = "UPDATE $this->Table SET iDeleted = 1 WHERE iContactID = ?";
        return $this->db->_query($sql, array($iContactID));
    }

    Public function CreateContact($iContactID)
    {

        //unsets IContactID from the array
        unset($this->arrFormElm["iContactID"]);
        unset($this->arrFormElm["daCreated"]);
        $f = array();
        //Makes a foreach that gets "filter" from $arrFormElm
        foreach ($this->arrFormElm as $key => $value) {
            $f[$key] = isset($_POST[$key]) ? $_POST[$key] : NULL;
            $f[$key] = filter_var($f[$key], $value["Filter"]);
        }

        $f["daStartDate"] = isset($f["daStartDate"]) ? strtotime($f["daStartDate"]) : time();
        $f["daExpireDate"] = isset($f["daExpireDate"]) ? strtotime($f["daExpireDate"]) : time() + (60 * 60 * 30);

        //uses the value of the "filter"'s and takes the value from the $_POST and inserts them into an array ordered by the "FIlTER"
        $params = array_values($f);

        //iContactID = IContactID
        $this->iContactID = $iContactID;

        //awesome SQL that makes an string that looks like this
        //"UPDATE Contact SET vcTitle = ?, vcLastName = ?, vcContactName = ?, vcPassword = ?, vcAddress = ?, iZip = ?, vcCity = ?, vcEmail = ?, vcPhone1 = ?, vcPhone2 = ? WHERE iContactID = 12"
        echo $sql = "INSERT INTO $this->Table SET " . implode(array_keys($this->arrFormElm), " = ?, ") . " = ?, daCreated = " . time();
        return $this->db->_query($sql, $params);
    }
}