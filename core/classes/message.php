<?php

/**
 * Created by PhpStorm.
 * Auther @ Mads Roloff - Rights reservede to Author
 * Date: 19-06-2017
 */
class message
{
    private $db;
    public $iMessageID = -1;
    public $vcImage;
    public $vcTitle;
    public $vcShortDesc;
    public $txLongDesc;

    public $daExpireDate;
    public $daStartDate;
    public $vcPlace;
    public $iDonate;
    public $vcDate;
    public $daCreated;
    public $iSuspended;
    public $iDeleted;
    Public $vcImagePath;

    public $arrFormElms = array();
    public $arrLabels = array();
    public $arrColumns = array();
    public $arrValues = array();
    public $unset = array();

    public function __construct()
    {
        global $db;
        $this->db = $db;
        //$this->CreateTable();
        $this->Table = "message";

        $this->arrFormElm = array(
            "iMessageID" => array("label" => "", "type" => "hidden", "placeholder" => "", "required" => "data-requried='1'", "dbname" => "iMessageID", "Filter" => FILTER_SANITIZE_NUMBER_INT),
            "vcTitle" => array("label" => "Message Titel", "type" => "text", "placeholder" => "Message Titel", "required" => "data-requried='1'", "dbname" => "vcTitle", "Filter" => FILTER_SANITIZE_STRING),
            "vcAddress" => array("label" => "Message Titel", "type" => "text", "placeholder" => "Message Titel", "required" => "data-requried='1'", "dbname" => "vcAddress", "Filter" => FILTER_SANITIZE_STRING),
            "iPhoneNumber" => array("label" => "Message Titel", "type" => "text", "placeholder" => "Message Titel", "required" => "data-requried='1'", "dbname" => "iPhoneNumber", "Filter" => FILTER_SANITIZE_NUMBER_INT),
            "txShortDesc" => array("label" => "Message Titel", "type" => "textarea", "placeholder" => "Message Titel", "required" => "data-requried='1'", "dbname" => "txShortDesc", "Filter" => FILTER_SANITIZE_STRING),


        );

    }

    public function GetSelect($limit = 100)
    {
        $sql = "SELECT * FROM $this->Table WHERE iDeleted = 0 LIMIT $limit";
        return $this->db->_fetch_array($sql, array());
    }

    public function GetList()
    {
        $sql = "SELECT * FROM $this->Table WHERE iDeleted = 0";
        //Shows Column names from "Message"
        $sqlLabels = "SHOW FULL COLUMNS FROM $this->Table";
        $this->arrLabels = $this->db->_fetch_array($sqlLabels, array());
        $this->arrValues = $this->db->_fetch_array($sql, array());

        //UNSET = Columns to avoid in LIST
        $this->unset = array(
            "iMessageID",
            "daCreated",
            "iSuspended",
            "iDeleted",

        );
    }

    public function getDetails($id)
    {
        $sql = "SELECT * FROM $this->Table WHERE iDeleted = 0 AND iMessageID = ?";
        //Shows Column names from "$this->table"
        $sqlLabels = "SHOW FULL COLUMNS FROM $this->Table";
        $this->arrLabels = $this->db->_fetch_array($sqlLabels, array());
        $this->arrValues = $this->db->_fetch_array($sql, array($id));

        //UNSET = Columns to avoid in LIST EDIT FOR DIFFRENT USE!
        $this->unset = array(
            "iMessageID",
            "daCreated",
            "iSuspended",
            "iDeleted",
        );
    }

    /**
     * Get Message
     * change $this->"class for use"
     * Used for edit mode
     */
    public function getMessage($id)
    {
        $this->iMessageID = $id;
        $sql = "SELECT * FROM $this->Table WHERE iDeleted = 0 AND $id = ?";
        $row = $this->db->_fetch_array($sql, array($id));
        foreach ($row as $key => $value) {
            $this->$key = $value;
        }
        return $row;
    }

    public function FooterMessage($limit)
    {
        $sql = "SELECT * FROM $this->Table WHERE iDeleted = 0 ORDER BY daCreated DESC LIMIT $limit";
        $row = $this->db->_fetch_array($sql, array());
        foreach ($row[0] as $key => $value) {
            $this->$key = $value;
        }
    }

    public function UpdateMessage($iMessageID)
    {
        //unsets IMessageID from the array
        unset($this->arrFormElm["iMessageID"]);

        //MAkes a foreach that gets "filter" from $arrFormElm
        foreach ($this->arrFormElm as $key => $value) {
            $f[$key] = filter_input(INPUT_POST, $key, $value["Filter"]);
        }
        //uses the value of the "filter"'s and takes the value from the $_POST and inserts them into an array ordered by the "FIlTER"
        $params = array_values($f);

        //iMessageID = IMessageID
        $this->iMessageID = $iMessageID;

        //awesome SQL that makes an string that looks like this
        //"UPDATE Message SET vcTitle = ?, vcLastName = ?, vcMessageName = ?, vcPassword = ?, vcAddress = ?, iZip = ?, vcCity = ?, vcEmail = ?, vcPhone1 = ?, vcPhone2 = ? WHERE iMessageID = 12"
        echo $sql = "UPDATE $this->Table SET " . implode(array_keys($this->arrFormElm), " = ?, ") . " = ? WHERE iMessageID = $this->iMessageID";
        return $this->db->_query($sql, $params);
    }

    public function DeleteMessage($iMessageID)
    {
        $this->iMessageID = $iMessageID;
        //SQL that sets iDeleted to 1
        $sql = "UPDATE $this->Table SET iDeleted = 1 WHERE iMessageID = ?";
        return $this->db->_query($sql, array($iMessageID));
    }

    Public function CreateMessage($iMessageID)
    {

        //unsets IMessageID from the array
        unset($this->arrFormElm["iMessageID"]);

        $f = array();
        //Makes a foreach that gets "filter" from $arrFormElm
        foreach ($this->arrFormElm as $key => $value) {
            $f[$key] = isset($_POST[$key]) ? $_POST[$key] : NULL;
            $f[$key] = filter_var($f[$key], $value["Filter"]);
        }

        //uses the value of the "filter"'s and takes the value from the $_POST and inserts them into an array ordered by the "FIlTER"
        $params = array_values($f);

        //iMessageID = IMessageID
        $this->iMessageID = $iMessageID;

        //awesome SQL that makes an string that looks like this
        //"UPDATE Message SET vcTitle = ?, vcLastName = ?, vcMessageName = ?, vcPassword = ?, vcAddress = ?, iZip = ?, vcCity = ?, vcEmail = ?, vcPhone1 = ?, vcPhone2 = ? WHERE iMessageID = 12"
        echo $sql = "INSERT INTO $this->Table SET " . implode(array_keys($this->arrFormElm), " = ?, ") . " = ?";
        return $this->db->_query($sql, $params);
    }
}