<?php

/**
 * Created by PhpStorm.
 * Auther @ Mads Roloff - Rights reservede to Author
 * Date: 19-06-2017
 */
class ingredeint
{
    private $db;
    public $iIngredeintID = -1;
    public $vcImage;
    public $vcTitle;
    public $vcShortDesc;
    public $txDesc;

    public $daExpireDate;
    public $daStartDate;
    public $vcPlace;
    public $iDonate;
    public $vcDate;
    public $daCreated;
    public $iSuspended;
    public $iDeleted;
    Public $vcImagePath;

    public $arrFormElms = array();
    public $arrLabels = array();
    public $arrColumns = array();
    public $arrValues = array();
    public $unset = array();

    public function __construct()
    {
        global $db;
        $this->db = $db;
        //$this->CreateTable();
        $this->Table = "ingredeints";

        $this->arrFormElm = array(
            "iIngredeintID" => array("label" => "", "type" => "hidden", "placeholder" => "", "required" => "data-requried='1'", "dbname" => "iIngredeintID", "Filter" => FILTER_SANITIZE_NUMBER_INT),
            "vcTitle" => array("label" => "ingredeint Titel", "type" => "text", "placeholder" => "ingredeint Titel", "required" => "data-requried='1'", "dbname" => "vcTitle", "Filter" => FILTER_SANITIZE_STRING),
            "txDesc" => array("label" => "Beskrivelse", "type" => "textarea", "placeholder" => "Beskrivelse", "required" => "data-requried='1'", "dbname" => "txDesc", "Filter" => FILTER_SANITIZE_STRING),
        );

    }

    public function GetSelect($limit = 100)
    {
        $sql = "SELECT * FROM $this->Table WHERE iDeleted = 0 LIMIT $limit";
        return $this->db->_fetch_array($sql, array());
    }

    public function GetList()
    {
        $sql = "SELECT * FROM $this->Table WHERE iDeleted = 0";
        //Shows Column names from "ingredeint"
        $sqlLabels = "SHOW FULL COLUMNS FROM $this->Table";
        $this->arrLabels = $this->db->_fetch_array($sqlLabels, array());
        $this->arrValues = $this->db->_fetch_array($sql, array());

        //UNSET = Columns to avoid in LIST
        $this->unset = array(
            "iIngredeintID",
            "daCreated",
            "iSuspended",
            "iDeleted",

        );
    }

    public function getAmount($iProductID)
    {
        $sql = "SELECT * FROM prodingrederel 
    INNER JOIN $this->Table on  prodingrederel.iIngredeintID = $this->Table.iIngredeintID
              WHERE iProductID = $iProductID";
        return $this->db->_fetch_array($sql);

    }

    public function getDetails($id)
    {
        $sql = "SELECT * FROM $this->Table WHERE iDeleted = 0 AND iIngredeintID = ?";
        //Shows Column names from "$this->table"
        $sqlLabels = "SHOW FULL COLUMNS FROM $this->Table";
        $this->arrLabels = $this->db->_fetch_array($sqlLabels, array());
        $this->arrValues = $this->db->_fetch_array($sql, array($id));

        //UNSET = Columns to avoid in LIST EDIT FOR DIFFRENT USE!
        $this->unset = array(
            "iIngredeintID",
            "daCreated",
            "iSuspended",
            "iDeleted",
        );
    }

    /**
     * Get ingredeint
     * change $this->"class for use"
     * Used for edit mode
     */
    public function getingredeint($id)
    {
        $this->iIngredeintID = $id;
        $sql = "SELECT * FROM $this->Table WHERE iDeleted = 0 AND $id = ?";
        $row = $this->db->_fetch_array($sql, array($id));
        foreach ($row as $key => $value) {
            $this->$key = $value;
        }
        return $row;
    }


    public function Updateingredeint($iIngredeintID)
    {
        //unsets iIngredeintID from the array
        unset($this->arrFormElm["iIngredeintID"]);

        //MAkes a foreach that gets "filter" from $arrFormElm
        foreach ($this->arrFormElm as $key => $value) {
            $f[$key] = filter_input(INPUT_POST, $key, $value["Filter"]);
        }
        //uses the value of the "filter"'s and takes the value from the $_POST and inserts them into an array ordered by the "FIlTER"
        $params = array_values($f);

        //iIngredeintID = iIngredeintID
        $this->iIngredeintID = $iIngredeintID;

        //awesome SQL that makes an string that looks like this
        //"UPDATE ingredeint SET vcTitle = ?, vcLastName = ?, vcingredeintName = ?, vcPassword = ?, vcAddress = ?, iZip = ?, vcCity = ?, vcEmail = ?, vcPhone1 = ?, vcPhone2 = ? WHERE iIngredeintID = 12"
        echo $sql = "UPDATE $this->Table SET " . implode(array_keys($this->arrFormElm), " = ?, ") . " = ? WHERE iIngredeintID = $this->iIngredeintID";
        return $this->db->_query($sql, $params);
    }

    public function Deleteingredeint($iIngredeintID)
    {
        $this->iIngredeintID = $iIngredeintID;
        //SQL that sets iDeleted to 1
        $sql = "UPDATE $this->Table SET iDeleted = 1 WHERE iIngredeintID = ?";
        return $this->db->_query($sql, array($iIngredeintID));
    }

    Public function Createingredeint($iIngredeintID)
    {
        //unsets iIngredeintID from the array
        unset($this->arrFormElm["iIngredeintID"]);
        $f = array();
        //Makes a foreach that gets "filter" from $arrFormElm
        foreach ($this->arrFormElm as $key => $value) {
            $f[$key] = isset($_POST[$key]) ? $_POST[$key] : NULL;
            $f[$key] = filter_var($f[$key], $value["Filter"]);
        }

        //uses the value of the "filter"'s and takes the value from the $_POST and inserts them into an array ordered by the "FIlTER"
        $params = array_values($f);


        //iIngredeintID = iIngredeintID
        $this->iIngredeintID = $iIngredeintID;

        //awesome SQL that makes an string that looks like this
        //"UPDATE ingredeint SET vcTitle = ?, vcLastName = ?, vcingredeintName = ?, vcPassword = ?, vcAddress = ?, iZip = ?, vcCity = ?, vcEmail = ?, vcPhone1 = ?, vcPhone2 = ? WHERE iIngredeintID = 12"
        $sql = "INSERT INTO $this->Table SET " . implode(array_keys($this->arrFormElm), " = ?, ") . " = ?, daCreated = " . time();
        return $this->db->_query($sql, $params);
    }
}