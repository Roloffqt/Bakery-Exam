<?php
/**
 * Created by PhpStorm.
 * Auther @ Mads Roloff - Rights reservede to Author
 * Date: 26-09-2017
 */

class comment
{
    private $db;
    public $keywords;

    public function __construct()
    {
        global $db;

        $this->db = $db;
    }

    public function postComment()
    {

        $iProductID = filter_input(INPUT_POST, "iProductID", FILTER_SANITIZE_NUMBER_INT);
        $iNewsID = filter_input(INPUT_POST, "iNewsID", FILTER_SANITIZE_NUMBER_INT);

        $iUserID = filter_input(INPUT_POST, "iUserID", FILTER_SANITIZE_NUMBER_INT);
        $vcTitle = filter_input(INPUT_POST, "vcTitle", FILTER_SANITIZE_STRING);
        $txContent = filter_input(INPUT_POST, "txContent", FILTER_SANITIZE_STRING);
        $vcImage = filter_input(INPUT_POST, "vcImage", FILTER_SANITIZE_STRING);

        $sql = "INSERT INTO comment (iProductID, iNewsID, iUserID, vcTitle, txContent, vcImage,daCreated)
                      VALUES('" . $iProductID . "','" . $iNewsID . "','" . (int)$iUserID . "', '" . (string)$vcTitle . "', '" . (string)$txContent . "', '" . (string)$vcImage . "','" . time() . "');";

        $this->db->_query($sql);

    }

    public function listComments($iProductID, $limit)
    {

        $sql = "SELECT * FROM comment WHERE iProductID = '" . (int)$iProductID . "' LIMIT " . $limit;

        $result = $this->db->_fetch_array($sql);

        return $result;
    }

    public function commentCount($iParentID)
    {
        $sql = "SELECT COUNT(iCommentID) FROM comment WHERE iProductID = " . (int)$iParentID;

        $result = $this->db->_fetch_value($sql);

        return $result;
    }

}