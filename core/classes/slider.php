<?php

/**
 * Created by PhpStorm.
 * Auther @ Mads Roloff - Rights reservede to Author
 * Date: 19-06-2017
 */
class slider
{
    private $db;
    public $iSlideID;
    public $vcImage;
    public $vcTitle;
    public $daCreated;
    public $iDeleted;


    public $arrFormElms = array();
    public $arrLabels = array();
    public $arrColumns = array();
    public $arrValues = array();
    public $unset = array();

    public function __construct()
    {
        global $db;
        $this->db = $db;
        //$this->CreateTable();
        $this->Table = "slider";

        $this->arrFormElm = array(
            "iSlideID" => array("label" => "", "type" => "hidden", "placeholder" => "", "required" => "data-requried='1'", "dbname" => "iSlideID", "Filter" => FILTER_SANITIZE_NUMBER_INT),
            "vcTitle" => array("label" => "slider Titel", "type" => "text", "placeholder" => "slider Titel", "required" => "data-requried='1'", "dbname" => "vcTitle", "Filter" => FILTER_SANITIZE_STRING),
            "vcImage" => array("label" => "", "type" => "hidden", "placeholder" => "Lang Beskrielvse", "required" => "data-requried='1'", "dbname" => "vcImage", "Filter" => FILTER_SANITIZE_STRING),
        );

    }

    public function GetSelect($limit = 100)
    {
        $sql = "SELECT * FROM $this->Table WHERE iDeleted = 0 LIMIT $limit";
        return $this->db->_fetch_array($sql, array());
    }

    public function GetList()
    {
        $sql = "SELECT * FROM $this->Table WHERE iDeleted = 0";
        //Shows Column names from "slider"
        $sqlLabels = "SHOW FULL COLUMNS FROM $this->Table";
        $this->arrLabels = $this->db->_fetch_array($sqlLabels, array());
        $this->arrValues = $this->db->_fetch_array($sql, array());

        //UNSET = Columns to avoid in LIST
        $this->unset = array(
            "iSlideID",
            "daCreated",
            "iSuspended",
            "iDeleted",

        );
    }

    public function getDetails($id)
    {
        $sql = "SELECT * FROM $this->Table WHERE iDeleted = 0 AND islideID = ?";
        //Shows Column names from "$this->table"
        $sqlLabels = "SHOW FULL COLUMNS FROM $this->Table";
        $this->arrLabels = $this->db->_fetch_array($sqlLabels, array());
        $this->arrValues = $this->db->_fetch_array($sql, array($id));

        //UNSET = Columns to avoid in LIST EDIT FOR DIFFRENT USE!
        $this->unset = array(
            "islideID",
            "daCreated",
            "iSuspended",
            "iDeleted",
        );
    }

    /**
     * Get slider
     * change $this->"class for use"
     * Used for edit mode
     */
    public function getslider($id)
    {
        $this->iSlideID = $id;
        $sql = "SELECT * FROM $this->Table WHERE iDeleted = 0 AND $id = ?";
        $row = $this->db->_fetch_array($sql, array($id));
        foreach ($row as $key => $value) {
            $this->$key = $value;
        }
        return $row;
    }


    public function Updateslider($islideID)
    {
        //unsets islideID from the array
        unset($this->arrFormElm["iSlideID"]);

        //MAkes a foreach that gets "filter" from $arrFormElm
        foreach ($this->arrFormElm as $key => $value) {
            $f[$key] = filter_input(INPUT_POST, $key, $value["Filter"]);
        }
        //uses the value of the "filter"'s and takes the value from the $_POST and inserts them into an array ordered by the "FIlTER"
        $params = array_values($f);

        //islideID = islideID
        $this->islideID = $islideID;

        //awesome SQL that makes an string that looks like this
        //"UPDATE slider SET vcTitle = ?, vcLastName = ?, vcsliderName = ?, vcPassword = ?, vcAddress = ?, iZip = ?, vcCity = ?, vcEmail = ?, vcPhone1 = ?, vcPhone2 = ? WHERE islideID = 12"
        echo $sql = "UPDATE $this->Table SET " . implode(array_keys($this->arrFormElm), " = ?, ") . " = ? WHERE iSlideID = $this->islideID";
        return $this->db->_query($sql, $params);
    }

    public function Deleteslider($islideID)
    {
        $this->islideID = $islideID;
        //SQL that sets iDeleted to 1
        $sql = "UPDATE $this->Table SET iDeleted = 1 WHERE islideID = ?";
        return $this->db->_query($sql, array($islideID));
    }

    Public function Createslider($islideID)
    {
        //unsets islideID from the array
        //unset($this->arrFormElm["iSlideID"]);
        $f = array();
        //Makes a foreach that gets "filter" from $arrFormElm
        foreach ($this->arrFormElm as $key => $value) {
            $f[$key] = isset($_POST[$key]) ? $_POST[$key] : NULL;
            $f[$key] = filter_var($f[$key], $value["Filter"]);
        }

        //uses the value of the "filter"'s and takes the value from the $_POST and inserts them into an array ordered by the "FIlTER"
        $params = array_values($f);


        //islideID = islideID
        $this->islideID = $islideID;

        //awesome SQL that makes an string that looks like this
        //"UPDATE slider SET vcTitle = ?, vcLastName = ?, vcsliderName = ?, vcPassword = ?, vcAddress = ?, iZip = ?, vcCity = ?, vcEmail = ?, vcPhone1 = ?, vcPhone2 = ? WHERE islideID = 12"
        $sql = "INSERT INTO $this->Table SET " . implode(array_keys($this->arrFormElm), " = ?, ") . " = ?, daCreated = " . time();
        return $this->db->_query($sql, $params);
    }
}