<?php
/**
 * Created by PhpStorm.
 * Auther @ Mads Roloff - Rights reservede to Author
 * Date: 13-09-2017
 */

/**
 * If else statement by Glen Jensen
 */
if (is_file(filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/init.php")) {
    require_once(filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/init.php");
} else {
    require_once(filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/public_html/assets/incl/init.php");
}

/**
 * Sets $_GET["message"] = null for the Signup for newsletter notifcation, since thats not made in ajax
 */


$slider = new slider();
$row = $slider->GetSelect(3);

$front = new frontpage();
$frontprint = $front->GetSelect();
sysHeader();

require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/nav.php";
?>
<div class="container-fluid">
    <section class="row">
        <div id="carousel" class="margin-top-xs-neg carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators ">
                <li data-target="#carousel" data-slide-to="0" class="bg-white active"></li>
                <li data-target="#carousel" data-slide-to="1" class="bg-white"></li>
                <li data-target="#carousel" data-slide-to="2" class="bg-white"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="carousel-item item active">


                    <div class="image" style="background-image: url('<?php echo $row[0]["vcImage"]; ?>');">
                        <p class="text-center carousel-text"><?php echo $row[0]["vcTitle"] ?></p>
                    </div>
                </div>

                <div class="carousel-item item">


                    <div class="image" style="background-image: url('<?php echo $row[1]["vcImage"]; ?>');">
                        <p class="text-center carousel-text"><?php echo $row[1]["vcTitle"] ?></p>
                    </div>
                </div>

                <div class="carousel-item item">

                    <div class="image" style="background-image: url('<?php echo $row[2]["vcImage"]; ?>');">
                        <p class="text-center carousel-text"><?php echo $row[2]["vcTitle"] ?></p>
                    </div>
                </div>
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#carousel" data-slide="prev">
                <i style="margin-top: 100%;" class="fa fa-chevron-left" aria-hidden="true"></i>
                <span class="sr-only">Previous</span>
            </a>
            <a class=" right carousel-control" href="#carousel" data-slide="next">
                <i style="margin-top: 100%;" class="fa fa-chevron-right" aria-hidden="true"></i>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </section>
</div>
<article class="container">
    <section class="row section-space">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <?php
            /**
             * if is set then show this alert
             */
            if (isset($_GET["message"])) { ?>
                <div class="margin-top-xs-pos alert alert-success">
                    <h2 class="text-center"><?php echo $_GET["message"] ?></h2>
                </div>
            <?php } ?>
            <div class="col-lg-offset-4 col-lg-4">
                <h2 class="logo-black text-center"><?php echo $frontprint[0]["vcTitle"] ?></h2>
            </div>
            <div class="col-lg-offset-3 col-lg-6 col-xs-12 col-sm-12">
                <p class="text-center"><?php echo $frontprint[0]["txContext"] ?></p>
            </div>
        </div>
    </section>
    <section class="row section-space">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-offset-2 col-lg-8">
                <?php
                $news = new news();
                $row = $news->GetSelect(3);

                foreach ($row as $value) {

                    ?>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="card text-center">
                            <div clasS="col-lg-12 col-md-12">
                                <img style="max-width: 100%; margin-bottom: 50px; border-radius:180px;" src="<?php echo $value["vcImage"] ?>" alt="Image">

                            </div>
                            <h5 style=""><strong><?php echo $value["vcTitle"] ?></strong></h5>

                            <p><?php echo substr($value["txShortDesc"], 1, max(150, $value["txShortDesc"])) ?></p>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </section>
</article>


<article class="container-fluid newsletter-bg">
    <section class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="margin-top-xs-pos padding-reset col-lg-8 col-md-8 col-sm-8 col-xs-12 col-sm-offset-2 col-md-offset-2   col-lg-offset-3">
                <h2 class="logo"><?php echo $frontprint[2]["vcTitle"] ?></h2>
                <div class="padding-reset col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <p class="font-color-white"><?php echo $frontprint[2]["txContext"] ?></p>

                </div>
            </div>
            <?php
            /**
             * Newsletter for, function is in
             * /public_html/assets/scripts/newsletter.php
             */
            ?>
            <div class="col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12">
                <form method="POST" action="assets/scripts/newsletter.php" data-validate class="input-item">
                    <div>
                        <div class="">
                            <label for="vcEmail"><i class="fa fa-envelope-o" aria-hidden="true"></i></label><input data-requried="1" name="vcEmail" type="email" placeholder="Indtast din email..." class="form-control" id="email">
                            <button class="" type="submit">Tilmeld</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </section>
</article>
<article class="container">
    <section class="row section-space">
        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
            <div class="col-lg-offset-2 col-lg-8 col-md-12 col-sm-12 col-xs-12 text-center">
                <h2 class="logo-black"><?php echo $frontprint[1]["vcTitle"] ?></h2>
                <p><?php echo $frontprint[1]["txContext"] ?></p>


                <?php
                $shopproduct = new shopproduct();
                $row = $shopproduct->GetSelect();

                //Shuffle makes $row's values come in random order so on every reload there will be a new way its ordered
                shuffle($row);

                foreach ($row as $value) {

                    $rows = $db->_fetch_value("SELECT COUNT(iLike) FROM productlike WHERE iProductID = " . $value["iProductID"]);

                    $comment = new comment();
                    $count = $comment->commentCount($value["iProductID"]);

                    ?>
                    <div class="col-lg-3 col-md-4 col-sm-4">
                        <div class="card text-center">
                            <div clasS="col-lg-12 col-md-12">
                                <img style="max-width: 100%; margin-bottom: 50px; border-radius:180px;" src="<?php echo $value["vcImage"] ?>" alt="Image">
                                <p class="comment-count"><?php echo $count ?><i class="fa fa-comment" aria-hidden="true"></i></p>
                                <a class="font-color-grey comment-amount" href="product.php?mode=Details&iProductID=<?php echo $value["iProductID"] ?>"><?php echo $rows ?>
                                    <i class="fa fa-thumbs-up" aria-hidden="true"></i></a>

                            </div>
                            <h5 style=""><strong><?php echo $value["vcTitle"] ?></strong></h5>

                            <p><?php echo substr($value["txDesc"], 0, max(40, $value["txDesc"])) ?></p>

                            <a href="product.php?mode=Details&iProductID=<?php echo $value["iProductID"] ?>" class="btn btn-default product-seemore">se mere</a>

                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </section>
</article>
<?php
sysFooter();
?>
