<?php
/**
 * Created by PhpStorm.
 * Auther @ Mads Roloff - Rights reservede to Author
 * Date: 25-09-2017
 */


if (is_file(filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/init.php")) {
    require_once(filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/init.php");
} else {
    require_once(filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/public_html/assets/incl/init.php");
}
$contact = new contact();

$row = $contact->GetSelect();

sysHeader();
?>
<div class="nav-bg">
    <?php
    require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/nav.php";
    //require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/SearchField.php";
    ?>
</div>
<article class="container">
    <section class="row section-space-med">
        <div class="col-lg-8 col-lg-offset-2 col-md-6 col-md-offset-3 col-sm-12 col-xs-12">
            <h1 class="logo-black text-center"> <?php echo $row[0]["vcTitle"] ?></h1>
            <p> <?php echo $row[0]["txShortDesc"] ?></p>
        </div>
    </section>
</article>

<?php

if (isset($_GET["message"])) { ?>
    <div class="section-space-med alert alert-success">
        <h2 class="text-center"><?php echo $_GET["message"] ?></h2>
    </div>
<?php } ?>
<article class="container contact">
    <section class="row">
        <div class="col-lg-offset-2 col-lg-4 col-md-8 col-md-offset-2 ">
            <?php
            /**
             * Form for sending message to Message tabel in db
             */
            ?>
            <form method="post" action="/assets/scripts/message.php" data-validate class="text-right">
                <div class="form-group">
                    <input data-requried="1" name="vcName" type="text" id="name" placeholder="Indtast dit navn" class="form-control">
                </div>
                <div class="form-group">
                    <input data-requried="1" name="vcEmail" type="email" id="email" placeholder="Indtast din email" class="form-control">
                </div>
                <div class="form-group">
                    <textarea data-requried="1" name="txMessage" placeholder="Indtast din besked" id="message" class="form-control" style="resize: none; height:200px;"></textarea>
                </div>
                <button name="submit" class="btn  contact-btn " type="submit">Send Besked</button>

            </form>
        </div>
        <div class="col-lg-4 col-md-8  ">

            <p><strong>Addresse</strong>: <?php echo $row[0]["vcAddress"] ?></p>
            <p><strong>Telefon</strong>: +45 <?php echo $row[0]["iPhoneNumber"] ?></p>
            <div id="map"></div>
        </div>
    </section>
</article>

<script>
     function initMap() {
          var uluru = {lat: 57.046707, lng: 9.935932};
          var map = new google.maps.Map(document.getElementById('map'), {
               zoom: 4,
               center: uluru
          });
          var marker = new google.maps.Marker({
               position: uluru,
               map: map
          });
     }
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB03gWxsDFNy1lNWVqNdLQSF3Ty5IICnww&callback=initMap">
</script>
<?php
sysFooter();
?>

