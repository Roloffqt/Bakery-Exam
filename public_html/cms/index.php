<?php
/**
 * Created by PhpStorm.
 * Auther @ Mads Roloff - Rights reservede to Author
 * Date: 17-05-2017
 */ ?>
<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/init.php";
sysBackendHeader();
if ($auth->user->sysadmin || $auth->user->admin) {
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/nav.php";


?>
<div class="">
    <div class="container">
        <div class="col-lg-12">
            <h1 class="text-center">Velkommen til Roloff-CMS <br><strong><?php echo $auth->user->vcUserName; ?></strong></h1>
            <hr style="background-color: black; height: 1px;">
            <ul class="text-center nav custom-nav">
                <li><a href="/cms/admin/upload.php?mode=list">Upload</a></li>
                <li><a href="/cms/admin/message.php?mode=list">Message</a></li>
                <li><a href="/cms/admin/user.php?mode=list">User</a></li>
                <li><a href="/cms/admin/usergroup.php?mode=list">User Groups</a></li>
                <li><a href="/cms/admin/News.php?mode=list">News</a></li>
                <li><a href="/cms/admin/slider.php?mode=list">Slider</a></li>
                <li><a href="/cms/admin/frontpage.php?mode=list">Frontpage</a></li>
                <li><a href="/cms/admin/contact.php?mode=list">Contact</a></li>
                <li><a href="/cms/admin/shopproduct.php?mode=list">Product</a></li>
                <li><a href="/cms/admin/shopcategory.php?mode=list">Category</a></li>
                <li><a href="/cms/admin/ingredeint.php?mode=list">Ingredeint</a></li>
            </ul>
            <hr style="background-color: black; height: 1px;">
        </div>
        <?php
        }
        /*if (!$auth->user->sysadmin || !$auth->user->admin) {
            echo "<div style='margin-top: 15%'>";
            echo "<h2 style='text-align: center'>Error 404</h2>";
            echo "<div class='col-lg-4 col-lg-offset-4'>";
            echo "<a style='text-align: center; width: 100%;' href='?action=logout' class='logout-btn btn btn-danger'> Log ind med en admin bruger</a>";
            echo "</div>";
            echo "</div>";
        }*/
        if (!$auth->checkSession()) {
            ?>


            <div class="col-lg-6 col-lg-offset-3" style="margin-top:5%;">
                <h2 class="text-center">Login</h2>
                <form id="loginform" method="post" autocomplete="off">

                    <div class="form-group">
                        <label for="username"> Username</label>
                        <input type="text" class="form-control" id="username" autocapitalize="none" name="username" placeholder="Indtast brugernavn" value="">
                    </div>

                    <div class="form-group">
                        <label for="password"> Password</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="password" value="">
                    </div>

                    <div class="btn-group col-lg-12">
                        <button type="submit" value="login" style="width: 100%;" class="btn btn-success login-button">Log ind</button>
                    </div>

                </form>
            </div>
            <?php
        }
        sysBackendFooter();

        ?>
