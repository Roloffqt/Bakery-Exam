<?php
/**
 * Created by PhpStorm.
 * Auther @ Mads Roloff - Rights reservede to Author
 * Date: 19-06-2017
 */ ?>


<?php if ($auth->user->sysadmin || $auth->user->admin) { ?>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a class="navbar-brand" href="/cms/index.php?mode=list">Roloff-cms</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="/cms/admin/imageUpload.php?mode=list">Upload</a></li>

                    <li><a href="/cms/admin/message.php">Message</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">User<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/cms/admin/user.php?mode=list">Brugere</a></li>
                            <li><a href="/cms/admin/usergroup.php?mode=list">Usergroups</a></li>

                        </ul>
                    </li>
                </ul>
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Content<span class="caret"></span></a>
                        <ul class="dropdown-menu">

                            <li><a href="/cms/admin/news.php?mode=list">News</a></li>
                            <li><a href="/cms/admin/slider.php?mode=list">Slider</a></li>
                            <li><a href="/cms/admin/frontpage.php?mode=list">Frontpage</a></li>
                            <li><a href="/cms/admin/contact.php?mode=list">Contact</a></li>
                        </ul>
                    </li>
                </ul>
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Produtker<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/cms/admin/shopcategory.php?mode=list">Category</a></li>
                            <li><a href="/cms/admin/shopproduct.php?mode=list">Shop Product</a></li>
                            <li><a href="/cms/admin/ingredeint.php?mode=list">Ingredeint</a></li>
                        </ul>
                    </li>
                </ul>

            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

<?php }