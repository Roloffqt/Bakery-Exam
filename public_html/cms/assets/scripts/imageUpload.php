<?php
/**
 * Created by PhpStorm.
 * Auther @ Mads Roloff - Rights reservede to Author
 * Date: 19-06-2017
 **/
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/init.php";

//default $file = $_files['"file"'];

/**
 * Change $_Files['Name of input field'] to whatever is fitting
 */
if (isset($_POST["submit"])) {
    $file = $_FILES['vcImage'];

    $fileName = $_FILES['vcImage']['name'];
    $filetmpName = $_FILES['vcImage']['tmp_name'];
    $fileSize = $_FILES['vcImage']['size'];
    $fileError = $_FILES['vcImage']['error'];
    $fileType = $_FILES['vcImage']['type'];


    $fileExT = explode('.', $fileName);
    $fileActualExt = strtolower(end($fileExT));


    /**
     * Change $allowed(array) add the file types you wanna allow to be uploaded
     */
    $allowed = array('jpg', 'jpeg', 'png', 'pdf');
    if ($_POST["vcTitle"] == NULL) {
        sysBackendHeader();
        require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/nav.php";
        echo "<h1 style='text-align:center;'>Billede navn er tomt!<br> <a style='text-align:center;' href='/cms/admin/imageUpload.php?mode=list\"'>Tilbage!</a></h1>";
        sysBackendFooter();
    } else {
        if (in_array($fileActualExt, $allowed)) {
            if ($fileError === 0) {


                /**
                 * $fileSize = max size of file thats uploadable
                 * $fileDestination = which folder the files are stored in, mostly a folder called "Uploads"
                 */
                if ($fileSize < 10000000) {
                    $fileNameNew = uniqid('', true) . "." . $fileActualExt;
                    $fileDestination = '/uploads/' . $fileNameNew;

                    move_uploaded_file($filetmpName, $_SERVER["DOCUMENT_ROOT"] . $fileDestination);

                    /**
                     * Change SQL statement so it fits the use for your current project
                     */
                    $sql = "INSERT INTO image(vcImagePath, vcTitle, daCreated) VALUES(?,?,?)";
                    $db->_query($sql, array(
                        $fileDestination,
                        $_POST["vcTitle"],
                        time()
                    ));


                    header("location: /./cms/admin/imageUpload.php?mode=list");

                } else {

                    /**
                     * File size Error
                     */
                    sysBackendHeader();
                    require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/nav.php";
                    echo "<h1 style='text-align: center;'>Your File is too big!</h1>";
                    echo "<h1 style='text-align: center;'>File has to be 10 Mb or less!</h1>";
                    sysBackendFooter();
                }
            } else {

                /**
                 * Error page
                 */
                sysBackendHeader();
                require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/nav.php";
                echo "<h1 style='text-align: center;'>There was an Error try agian</h1>";
                sysBackendFooter();
            }
        } else {

            /**
             * File format Error
             */
            sysBackendHeader();
            require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/nav.php";
            echo "<h1 style='text-align: center;'>Wrong file format!</h1>";
            echo "<h2 style='text-align: center;'>You can only upload 'jpg', 'jpeg', 'png', 'pdf' </h2>";
            sysBackendFooter();
        }

    }
}
?>

