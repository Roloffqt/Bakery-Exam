<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/init.php";
$mode = setMode();
$strModuleName = "Shop product";
if ($auth->user->sysadmin || $auth->user->admin) {
    switch (strtoupper($mode)) {

        case "LIST";
            $iParentID = filter_input(INPUT_GET, "iParentID", FILTER_SANITIZE_NUMBER_INT, getDefaultValue(-1));

            $strModuleMode = "Oversigt";
            sysBackendHeader();
            require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/nav.php";

            $Product = new shopproduct();
            $Product->GetList();

            $iProductID = filter_input(INPUT_GET, "iProductID", FILTER_SANITIZE_NUMBER_INT);

            ?>
            <div class="container">
                <div class="row">
                    <?php
                    $arrButtonPanel = array();
                    echo textpresenter::presentMode($strModuleName, $strModuleMode, $arrButtonPanel);

                    ?>

                    <div class="col-md-12">
                        <h4>Produtker Table</h4>
                        <a class="btn btn-success" href="shopproduct.php?mode=Edit&iProductID=-1">Opret nyt Produkt!</a>
                        <div class="table-responsive">

                            <table class="table table-bordred table-striped">
                                <?php
                                $p = new listPresenter();
                                $p->ListMaker($Product->arrLabels, $Product->unset, $Product->arrValues, "iProductID");
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


            <?php
            sysBackendfooter();
            break;

        case "DETAILS":

            $iProductID = filter_input(INPUT_GET, "iProductID", FILTER_SANITIZE_NUMBER_INT);
            $strModuleMode = "Detaljer";
            sysBackendHeader();
            require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/nav.php";
            $Product = new shopProduct();
            $Product->getDetails($iProductID);
            ?>

            <div class="container">
                <div class="row">
                    <?php
                    $arrButtonPanel = array();
                    $arrButtonPanel[] = getButtonLink("table", "?mode=list", "Oversigt", "btn-primary");
                    $arrButtonPanel[] = getButtonLink("pencil", "?mode=edit&iProductID=" . $iProductID, "Rediger side", "btn-success");
                    $arrButtonPanel[] = getButtonLink("plus", "?mode=SETCATEGORY&iProductID=" . $iProductID, "Vælg Grupper", "btn");
                    $arrButtonPanel[] = getButtonLink("plus", "?mode=SETINGREDEINTS&iProductID=" . $iProductID, "Vælg ingredeints", "btn");
                    echo textpresenter::presentMode($strModuleName, $strModuleMode, $arrButtonPanel);
                    ?>
                    <div class="col-md-12">
                        <h4>Produkt Details</h4>
                        <div class="table-responsive">


                            <table class="table table-bordred table-striped">
                                <?php
                                $list = new ListPresenter();
                                $list->MakeDetails($Product->arrLabels, $Product->unset, $Product->arrValues);
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            sysBackendfooter();
            break;

        case "EDIT";

            $Product = new shopProduct();
            $fields = $Product->arrFormElm;
            $iProductID = filter_input(INPUT_GET, "iProductID", FILTER_SANITIZE_NUMBER_INT);
            $iParentID = filter_input(INPUT_GET, "iParentID", FILTER_SANITIZE_NUMBER_INT, getDefaultValue(-1));


            //Makes $select into image parameter!
            $image = new image();
            $selectimg = $image->GetSelect();

            sysBackendHeader();
            require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/nav.php";
            $strModuleMode = ($iProductID > 0) ? "Rediger" : "Opret ny side";


            ?>
            <div class="container">
                <div class="row">
                    <?php

                    $arrButtonPanel = array();
                    $arrButtonPanel[] = getButtonLink("table", "?mode=list", "Oversigt", "btn-primary");
                    echo textpresenter::presentMode($strModuleName, $strModuleMode, $arrButtonPanel);

                    ?>
                    <div class="col-md-12">

                        <?php
                        if ($iProductID == -1) {
                            ?>
                            <h4>Opret Produkt!</h4>
                            <?php
                        } else {
                            ?>
                            <h4>Rediger Produkt!</h4>
                            <?php
                        }

                        //get the item ID;
                        if ($iProductID > 0) {
                            $Product->getProduct($iProductID);
                        }

                        $form = new formpresenter();
                        $form->MakeForm($fields, $Product, $selectimg, false, "shopProduct", false, true, true, false);
                        ?>
                    </div>
                </div>
            </div>

            <?php
            sysBackendfooter();
            break;


        case "SETINGREDEINTS":
            $iProductID = isset($_GET["iProductID"]) && is_int(intval($_GET["iProductID"])) ? $_GET["iProductID"] : false;


            $strModuleMode = "INGREDEINTS";
            sysBackendHeader();
            require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/nav.php";

            $arrSelected = array();
            $params = array($iProductID);

            $strSelect = "SELECT iIngredeintID, iAmount FROM prodingrederel WHERE iProductID = ?";
            $rel = $db->_fetch_array($strSelect, $params);

            foreach ($rel as $value) {
                $arrSelected[] = $value["iIngredeintID"];
                $arrSelected[] = $value["iAmount"];

            }


            $cate = new ingredeint();
            $rows = $cate->getSelect();


            $arrFormValues = array();
            $arrFormValues["iProductID"] = $iProductID;

            ?>

            <div class="container">
                <div class="row">

                    <div class="col-md-12">
                        <form method="POST">
                            <input type='hidden' name='mode' value='SAVEINGREDEINTS'>
                            <?php
                            $arrButtonPanel = array();
                            $arrButtonPanel[] = getButtonLink("plus", "?mode=list", "Oversigt", "btn");
                            echo textPresenter::presentMode($strModuleName, $strModuleMode, $arrButtonPanel);

                            foreach ($rows as $key => $arrValues) {
                                $field = $arrValues["iIngredeintID"];
                                $arrFormValues[$field] = in_array($arrValues["iIngredeintID"], $arrSelected) ? 1 : 0;
                                $strIsChecked = $arrFormValues[$field] ? "checked=\"checked\"" : "";
                                ?>
                                <div class="col-md-12 form-group">
                                    <label>
                                        <?php echo $arrValues["vcTitle"];
                                        ?>
                                        <input type="checkbox" name="groups[]" value="<?php echo $field; ?>" <?php echo $strIsChecked ?>>
                                        <input type="text" name="ingredeints[<?php echo $arrValues["iIngredeintID"]; ?>]" placeholder="amount" value="">
                                    </label>
                                </div>
                                <?php
                            } ?>
                            <button type="submit" name='submit' onclick="validate(this.form)" class="btn btn-success">GEM</button>
                            <a href='?mode=list' class="btn btn-danger">Tilbage</a>
                        </form>
                    </div>
                </div>
            </div>

            <?php


            sysBackendFooter();
            break;
        case "SAVEINGREDEINTS":


            $iProductID = Filter_input(INPUT_GET, "iProductID", FILTER_SANITIZE_NUMBER_INT);
            $params = array($iProductID);
            $strDelete = "DELETE FROM prodingrederel WHERE iProductID = ?";
            $db->_query($strDelete, $params);

            $args = array(
                "ingredeints" => array(
                    "filter" => FILTER_SANITIZE_STRING,
                    "flags" => FILTER_REQUIRE_ARRAY
                ),
                "groups" => array(
                    "filter" => FILTER_VALIDATE_INT,
                    "flags" => FILTER_REQUIRE_ARRAY
                )
            );
            $arrInputVal = filter_input_array(INPUT_POST, $args);
            $arrclean = array_filter($arrInputVal["ingredeints"], 'strlen');
            count($arrInputVal["groups"]);
            count($arrclean);


            if (count($arrInputVal["groups"]) == count($arrclean)) {

                $arrGroups = array_values($arrInputVal["groups"]);
                $arrIng = array_values($arrInputVal["ingredeints"]);

                $arrcombined = array_combine($arrGroups, $arrclean);

                foreach ($arrcombined as $key => $value) {

                    $params = array($iProductID, $key, $value);
                    echo $strInsert = "INSERT INTO prodingrederel(iProductID, iIngredeintID, iAmount) VALUES(?,?,?)";
                    $db->_query($strInsert, $params);
                }

            }


            header("location: ?mode=details&iProductID=" . $iProductID);

            break;

        case
        "SETCATEGORY":
            $iProductID = isset($_GET["iProductID"]) && is_int(intval($_GET["iProductID"])) ? $_GET["iProductID"] : false;


            $strModuleMode = "Category";
            sysBackendHeader();
            require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/nav.php";

            $arrSelected = array();
            $params = array($iProductID);

            $strSelect = "SELECT iCategoryID FROM shopcatprodrel WHERE iProductID = ?";
            $rel = $db->_fetch_array($strSelect, $params);

            foreach ($rel as $value) {
                $arrSelected[] = $value["iCategoryID"];
            }


            $cate = new shopcategory();
            $rows = $cate->getSelect();


            $arrFormValues = array();
            $arrFormValues["iProductID"] = $iProductID;


            ?>

            <div class="container">
                <div class="row">

                    <div class="col-md-12">
                        <form method="POST">
                            <input type='hidden' name='mode' value='SAVECATEGORY'>
                            <?php
                            $arrButtonPanel = array();
                            $arrButtonPanel[] = getButtonLink("plus", "?mode=list", "Oversigt", "btn");
                            echo textPresenter::presentMode($strModuleName, $strModuleMode, $arrButtonPanel);

                            foreach ($rows as $key => $arrValues) {
                                $field = $arrValues["iCategoryID"];
                                $arrFormValues[$field] = in_array($arrValues["iCategoryID"], $arrSelected) ? 1 : 0;
                                $strIsChecked = $arrFormValues[$field] ? "checked=\"checked\"" : "";
                                ?>
                                <div class="col-md-12 form-group">
                                    <label>
                                        <?php echo $arrValues["vcTitle"] ?>
                                        <input type="checkbox" name="groups[]" value="<?php echo $field; ?>" <?php echo $strIsChecked ?>>
                                    </label>
                                </div>
                                <?php
                            } ?>
                            <button type="submit" name='submit' onclick="validate(this.form)" class="btn btn-success">GEM</button>
                            <a href='?mode=list' class="btn btn-danger">Tilbage</a>
                        </form>
                    </div>
                </div>
            </div>

            <?php


            sysBackendFooter();
            break;


        case "SAVECATEGORY":
            showme($_POST);

            $iProductID = Filter_input(INPUT_GET, "iProductID", FILTER_SANITIZE_NUMBER_INT);
            $params = array($iProductID);
            $strDelete = "DELETE FROM shopcatprodrel WHERE iProductID = ?";
            $db->_query($strDelete, $params);

            $args = array(
                "groups" => array(
                    "filter" => FILTER_VALIDATE_INT,
                    "flags" => FILTER_REQUIRE_ARRAY
                )
            );
            $arrInputVal = filter_input_array(INPUT_POST, $args);

            if (count($arrInputVal["groups"])) {

                $arrGroups = array_values($arrInputVal["groups"]);

                foreach ($arrGroups as $value) {
                    $params = array($iProductID, $value);
                    $strInsert = "INSERT INTO shopcatprodrel(iProductID, iCategoryID) VALUES(?,?)";
                    $db->_query($strInsert, $params);
                }
            }
            header("location: ?mode=details&iProductID=" . $iProductID);

            break;


        case "DELETE":
            $obj = new shopproduct();
            $id = filter_input(INPUT_GET, "iProductID", FILTER_VALIDATE_INT);
            $obj->delete($id);
            header("Location: ?mode=list");
            break;


        case "SAVE":
            $id = filter_input(INPUT_POST, "iProductID", FILTER_SANITIZE_NUMBER_INT);

            $obj = new shopproduct();

            showme($_POST);
            if ($id > 0) {
                //Runs $obj->DeleteEvent on load
                $obj->UpdateObj($id);
                //redicrects to Mode list after running DeleteEvent
            } else {
                //Runs $obj->DeleteEvent on load
                $obj->CreateObj($id);
                //redicrects to Mode list after running DeleteEvent
            }
            //header("location: ?mode=list");
            break;

    }
} else {
    header("location: /cms/Calendar.php");
}
