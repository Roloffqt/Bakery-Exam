<?php
/**
 * Created by PhpStorm.
 * Auther @ Mads Roloff - Rights reservede to Author
 * Date: 17-05-2017
 */ ?>
<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/init.php";
$mode = setMode();
$strModuleName = "Brugere";
if ($auth->user->sysadmin || $auth->user->admin) {
    switch (strtoupper($mode)) {

        Default:
            sysBackendHeader();
            require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/nav.php";
            $Contact = new Contact();
            $Contact->GetList();

            $iContactID = filter_input(INPUT_GET, "iContactID", FILTER_SANITIZE_NUMBER_INT);
            ?>
            <div class="container">
                <div class="row">

                    <div class="col-md-12">
                        <h4>Contact Table</h4>
                        <div class="table-responsive">

                            <table class="table table-bordred table-striped">
                                <?php
                                $list = new listpresenter();
                                $list->ListMaker($Contact->arrLabels, $Contact->unset, $Contact->arrValues, "iContactID", true, false);
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


            <?php


            sysBackendFooter();
            break;

        case "DETAILS":
            sysBackendHeader();
            require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/nav.php";
            $Contact = new Contact();

            $iContactID = filter_input(INPUT_GET, "iContactID", FILTER_SANITIZE_NUMBER_INT);

            $Contact->getDetails($iContactID);
            ?>
            <div class="container">
                <div class="row">

                    <div class="col-md-12">
                        <h4>Contact Details</h4>
                        <div class="table-responsive">


                            <table class="table table-bordred table-striped">
                                <?php
                                $list = new listpresenter();
                                $list->MakeDetails($Contact->arrLabels, $Contact->unset, $Contact->arrValues);
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <?php
            sysBackendFooter();
            break;

        case "EDIT":
            sysBackendHeader();
            require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/nav.php";
            $Contact = new Contact();
            $fields = $Contact->arrFormElm;
            $iContactID = filter_input(INPUT_GET, "iContactID", FILTER_SANITIZE_NUMBER_INT);

            $image = new image();
            $select = $image->GetSelect();

            ?>
            <div class="container">
                <div class="row">


                    <div class="col-md-12">


                        <?php
                        if ($iContactID == -1) {
                            ?>
                            <h4>Opret Contact!</h4>
                            <?php
                        } else {
                            ?>
                            <h4>Rediger Contact!</h4>
                            <?php
                        }
                        //get the item ID;
                        if ($iContactID == 0 || $iContactID == "-1") {
                        } else {
                            $Contact->getContact($iContactID);
                        }


                        $form = new FormPresenter();
                        $form->MakeForm($fields, $Contact, $select, null, "Contact", false, false, true);


                        ?>
                    </div>
                </div>
            </div>

            <?php
            sysBackendFooter();
            break;

        case
        "DELETE":
            $obj = new Contact();
            $id = filter_input(INPUT_GET, "iContactID", FILTER_VALIDATE_INT);
            //Runs $obj->DeleteContact on load
            $obj->DeleteContact($id);
            //redicrects to Mode list after running DeleteContact
            header("location: ?mode=list");

            break;
        case "SAVE":
            $id = filter_input(INPUT_POST, "iContactID", FILTER_SANITIZE_NUMBER_INT);


            if ($id > 0) {

                $obj = new Contact();
                //Runs $obj->DeleteContact on load
                $obj->UpdateContact($id);
                //redicrects to Mode list after running DeleteContact

            }
            if ($id == -1) {

                showme($_POST);
                $obj = new Contact();
                //Runs $obj->DeleteContact on load
                $obj->CreateContact($id);
                //redicrects to Mode list after running DeleteContact

            }

            header("location: ?mode=list");
            break;
    }
} else {
    header("location: /cms/Calendar.php");
}

