<?php
/**
 * Created by PhpStorm.
 * Auther @ Mads Roloff - Rights reservede to Author
 * Date: 17-05-2017
 */ ?>
<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/init.php";
$mode = setMode();
$strModuleName = "Brugere";
if ($auth->user->sysadmin || $auth->user->admin) {
    switch (strtoupper($mode)) {

        Default:
            sysBackendHeader();
            require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/nav.php";
            $ingredeint = new ingredeint();
            $ingredeint->GetList();

            $iIngredeintID = filter_input(INPUT_GET, "iIngredeintID", FILTER_SANITIZE_NUMBER_INT);
            ?>
            <div class="container">
                <div class="row">

                    <div class="col-md-12">
                        <h4>ingredeint Table</h4>
                        <a class="btn btn-success" href="ingredeint.php?mode=Edit&iIngredeintID=-1">Opret nyt ingredeint!</a>
                        <div class="table-responsive">

                            <table class="table table-bordred table-striped">
                                <?php
                                $list = new listpresenter();
                                $list->ListMaker($ingredeint->arrLabels, $ingredeint->unset, $ingredeint->arrValues, "iIngredeintID", true);
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


            <?php


            sysBackendFooter();
            break;

        case "DETAILS":
            sysBackendHeader();
            require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/nav.php";
            $ingredeint = new ingredeint();

            $iIngredeintID = filter_input(INPUT_GET, "iIngredeintID", FILTER_SANITIZE_NUMBER_INT);

            $ingredeint->getDetails($iIngredeintID);
            ?>
            <div class="container">
                <div class="row">

                    <div class="col-md-12">
                        <h4>ingredeint Details</h4>
                        <div class="table-responsive">


                            <table class="table table-bordred table-striped">
                                <?php
                                $list = new listpresenter();
                                $list->MakeDetails($ingredeint->arrLabels, $ingredeint->unset, $ingredeint->arrValues);
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <?php
            sysBackendFooter();
            break;

        case "EDIT":
            sysBackendHeader();
            require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/nav.php";
            $ingredeint = new ingredeint();
            $fields = $ingredeint->arrFormElm;
            $iIngredeintID = filter_input(INPUT_GET, "iIngredeintID", FILTER_SANITIZE_NUMBER_INT);

            $image = new image();
            $select = $image->GetSelect();
            ?>

            <div class="container">
                <div class="row">

                    <div class="col-md-12">

                        <?php
                        if ($iIngredeintID == -1) {
                            ?>
                            <h4>Opret ingredeint!</h4>
                            <?php
                        } else {
                            ?>
                            <h4>Rediger ingredeint!</h4>
                            <?php
                        }
                        //get the item ID;
                        if ($iIngredeintID == 0 || $iIngredeintID == "-1") {
                        } else {
                            $ingredeint->getingredeint($iIngredeintID);
                        }
                        $form = new FormPresenter();
                        $form->MakeForm($fields, $ingredeint, $select, null, "ingredeint", false, false, false);


                        ?>
                    </div>
                </div>
            </div>

            <?php
            sysBackendFooter();
            break;


        case
        "DELETE":
            $obj = new ingredeint();
            $id = filter_input(INPUT_GET, "iIngredeintID", FILTER_VALIDATE_INT);
            //Runs $obj->Deleteingredeint on load
            $obj->Deleteingredeint($id);
            //redicrects to Mode list after running Deleteingredeint
            header("location: ?mode=list");

            break;
        case "SAVE":
            $id = filter_input(INPUT_POST, "iIngredeintID", FILTER_SANITIZE_NUMBER_INT);


            if ($id > 0) {

                $obj = new ingredeint();
                //Runs $obj->Deleteingredeint on load
                $obj->Updateingredeint($id);
                //redicrects to Mode list after running Deleteingredeint

            }
            if ($id == -1) {
                showme($_POST);
                $obj = new ingredeint();
                //Runs $obj->Deleteingredeint on load
                $obj->Createingredeint($id);
                //redicrects to Mode list after running Deleteingredeint

            }

            header("location: ?mode=list");
            break;
    }
} else {
    header("location: /cms/Calendar.php");
}

