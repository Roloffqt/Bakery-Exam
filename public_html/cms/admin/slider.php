<?php
/**
 * Created by PhpStorm.
 * Auther @ Mads Roloff - Rights reservede to Author
 * Date: 17-05-2017
 */ ?>
<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/init.php";
$mode = setMode();
$strModuleName = "Brugere";
if ($auth->user->sysadmin || $auth->user->admin) {
    switch (strtoupper($mode)) {

        Default:
            sysBackendHeader();
            require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/nav.php";
            $slider = new slider();
            $slider->GetList();

            $iSlideID = filter_input(INPUT_GET, "iSlideID", FILTER_SANITIZE_NUMBER_INT);
            ?>
            <div class="container">
                <div class="row">

                    <div class="col-md-12">
                        <h4>Slider Table</h4>
                        <div class="table-responsive">

                            <table class="table table-bordred table-striped">
                                <?php
                                $list = new listpresenter();
                                $list->ListMaker($slider->arrLabels, $slider->unset, $slider->arrValues, "iSlideID", true, false);
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


            <?php


            sysBackendFooter();
            break;

        case "DETAILS":
            sysBackendHeader();
            require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/nav.php";
            $slider = new slider();

            $iSlideID = filter_input(INPUT_GET, "iSlideID", FILTER_SANITIZE_NUMBER_INT);

            $slider->getDetails($iSlideID);
            ?>
            <div class="container">
                <div class="row">

                    <div class="col-md-12">
                        <h4>Bruger Details</h4>
                        <div class="table-responsive">


                            <table class="table table-bordred table-striped">
                                <?php
                                $list = new listpresenter();
                                $list->MakeDetails($slider->arrLabels, $slider->unset, $slider->arrValues);
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <?php
            sysBackendFooter();
            break;

        case "EDIT":
            sysBackendHeader();
            $slider = new slider();

            $fields = $slider->arrFormElm;

            $iSlideID = filter_input(INPUT_GET, "iSlideID", FILTER_SANITIZE_NUMBER_INT);

            require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/nav.php";

            $image = new image();
            $select = $image->GetSelect();
            ?>

            <div class="container">
                <div class="row">

                    <div class="col-md-12">

                        <?php
                        if ($iSlideID == -1) {
                            ?>
                            <h4>Opret Slide!</h4>
                            <?php
                        } else {
                            ?>
                            <h4>Rediger Slide!</h4>
                            <?php
                        }
                        //get the item ID;
                        if ($iSlideID >= 0) {
                            $slider->getslider($iSlideID);
                        }
                        $form = new FormPresenter();
                        $form->MakeForm($fields, $slider, $select, null, "slider", false, false, true);


                        ?>
                    </div>
                </div>
            </div>

            <?php
            sysBackendFooter();
            break;

        case
        "DELETE":
            $obj = new slider();
            $id = filter_input(INPUT_GET, "iSlideID", FILTER_VALIDATE_INT);
            //Runs $obj->Deleteslider on load
            $obj->Deleteslider($id);
            //redicrects to Mode list after running Deleteslider
            header("location: ?mode=list");

            break;
        case "SAVE":
            unset($_POST["iSlideID"]);
            $id = filter_input(INPUT_GET, "iSlideID", FILTER_SANITIZE_NUMBER_INT);
            showme($_POST);

            if ($id > 0) {

                $obj = new slider();
                //Runs $obj->Deleteslider on load
                $obj->Updateslider($id);
                //redicrects to Mode list after running Deleteslider

            }
            if ($id == -1) {


                exit();
                $obj = new slider();
                //Runs $obj->Deleteslider on load
                $obj->Createslider($id);
                //redicrects to Mode list after running Deleteslider

            }

            header("location: ?mode=list");
            break;
    }
} else {
    header("location: /cms/Calendar.php");
}

