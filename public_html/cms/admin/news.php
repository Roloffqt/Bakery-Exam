<?php
/**
 * Created by PhpStorm.
 * Auther @ Mads Roloff - Rights reservede to Author
 * Date: 17-05-2017
 */ ?>
<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/init.php";
$mode = setMode();
$strModuleName = "Brugere";
if ($auth->user->sysadmin || $auth->user->admin) {
    switch (strtoupper($mode)) {

        Default:
            sysBackendHeader();
            require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/nav.php";
            $News = new News();
            $News->GetList();

            $iNewsID = filter_input(INPUT_GET, "iNewsID", FILTER_SANITIZE_NUMBER_INT);
            ?>
            <div class="container">
                <div class="row">

                    <div class="col-md-12">
                        <h4>Nyheds Table</h4>
                        <a class="btn btn-success" href="news.php?mode=Edit&iNewsID=-1">Opret nyt News!</a>
                        <div class="table-responsive">

                            <table class="table table-bordred table-striped">
                                <?php
                                $list = new listpresenter();
                                $list->ListMaker($News->arrLabels, $News->unset, $News->arrValues, "iNewsID");
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


            <?php


            sysBackendFooter();
            break;

        case "DETAILS":
            sysBackendHeader();
            require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/nav.php";
            $News = new News();

            $iNewsID = filter_input(INPUT_GET, "iNewsID", FILTER_SANITIZE_NUMBER_INT);

            $News->getDetails($iNewsID);
            ?>
            <div class="container">
                <div class="row">

                    <div class="col-md-12">
                        <h4>Bruger Details</h4>
                        <div class="table-responsive">


                            <table class="table table-bordred table-striped">
                                <?php
                                $list = new listpresenter();
                                $list->MakeDetails($News->arrLabels, $News->unset, $News->arrValues);
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <?php
            sysBackendFooter();
            break;

        case "EDIT":
            sysBackendHeader();
            require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/nav.php";
            $News = new News();
            $fields = $News->arrFormElm;
            $iNewsID = filter_input(INPUT_GET, "iNewsID", FILTER_SANITIZE_NUMBER_INT);

            $image = new image();
            $select = $image->GetSelect();

            ?>
            <div class="container">
                <div class="row">


                    <div class="col-md-12">


                        <?php
                        if ($iNewsID == -1) {
                            ?>
                            <h4>Opret Nyhed!</h4>
                            <?php
                        } else {
                            ?>
                            <h4>Rediger Nyhed!</h4>
                            <?php
                        }
                        //get the item ID;
                        if ($iNewsID == 0 || $iNewsID == "-1") {
                        } else {
                            $News->getNews($iNewsID);
                        }

                        $form = new FormPresenter();
                        $form->MakeForm($fields, $News, $select, null, "News", false, false, true);


                        ?>
                    </div>
                </div>
            </div>

            <?php
            sysBackendFooter();
            break;

        case
        "DELETE":
            $obj = new news();
            $id = filter_input(INPUT_GET, "iNewsID", FILTER_VALIDATE_INT);
            //Runs $obj->DeleteNews on load
            $obj->DeleteNews($id);
            //redicrects to Mode list after running DeleteNews
            header("location: ?mode=list");

            break;
        case "SAVE":
            $id = filter_input(INPUT_POST, "iNewsID", FILTER_SANITIZE_NUMBER_INT);


            if ($id > 0) {

                $obj = new news();
                //Runs $obj->DeleteNews on load
                $obj->UpdateNews($id);
                //redicrects to Mode list after running DeleteNews

            }
            if ($id == -1) {

                showme($_POST);
                $obj = new news();
                //Runs $obj->DeleteNews on load
                $obj->CreateNews($id);
                //redicrects to Mode list after running DeleteNews

            }

            header("location: ?mode=list");
            break;
    }
} else {
    header("location: /cms/Calendar.php");
}

