<?php
/**
 * Created by PhpStorm.
 * Auther @ Mads Roloff - Rights reservede to Author
 * Date: 17-05-2017
 */ ?>
<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/init.php";
$mode = setMode();
$strModuleName = "Brugere";
if ($auth->user->sysadmin || $auth->user->admin) {
    switch (strtoupper($mode)) {

        Default:
            sysBackendHeader();
            require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/nav.php";
            $Message = new message();
            $Message->GetList();

            $iMessageID = filter_input(INPUT_GET, "iMessageID", FILTER_SANITIZE_NUMBER_INT);
            ?>
            <div class="container">
                <div class="row">

                    <div class="col-md-12">
                        <h4>Message Table</h4>
                        <div class="table-responsive">

                            <table class="table table-bordred table-striped">
                                <?php
                                $list = new listpresenter();
                                $list->ListMaker($Message->arrLabels, $Message->unset, $Message->arrValues, "iMessageID", false, true);
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


            <?php


            sysBackendFooter();
            break;

        case "DETAILS":
            sysBackendHeader();
            require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/nav.php";
            $Message = new Message();

            $iMessageID = filter_input(INPUT_GET, "iMessageID", FILTER_SANITIZE_NUMBER_INT);

            $Message->getDetails($iMessageID);
            ?>
            <div class="container">
                <div class="row">

                    <div class="col-md-12">
                        <h4>
                            <Beskeder></Beskeder>
                        </h4>
                        <div class="table-responsive">


                            <table class="table table-bordred table-striped">
                                <?php
                                $list = new listpresenter();
                                $list->MakeDetails($Message->arrLabels, $Message->unset, $Message->arrValues);
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <?php
            sysBackendFooter();
            break;

        case "EDIT":
            sysBackendHeader();
            require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/nav.php";
            $Message = new Message();
            $fields = $Message->arrFormElm;
            $iMessageID = filter_input(INPUT_GET, "iMessageID", FILTER_SANITIZE_NUMBER_INT);

            $image = new image();
            $select = $image->GetSelect();

            ?>
            <div class="container">
                <div class="row">


                    <div class="col-md-12">


                        <?php
                        if ($iMessageID == -1) {
                            ?>
                            <h4>Opret bruger!</h4>
                            <?php
                        } else {
                            ?>
                            <h4>Rediger bruger!</h4>
                            <?php
                        }
                        //get the item ID;
                        if ($iMessageID == 0 || $iMessageID == "-1") {
                        } else {
                            $Message->getMessage($iMessageID);
                        }


                        $form = new FormPresenter();
                        $form->MakeForm($fields, $Message, $select, null, "Message", false, false, true);


                        ?>
                    </div>
                </div>
            </div>

            <?php
            sysBackendFooter();
            break;

        case
        "DELETE":
            $obj = new Message();
            $id = filter_input(INPUT_GET, "iMessageID", FILTER_VALIDATE_INT);
            //Runs $obj->DeleteMessage on load
            $obj->DeleteMessage($id);
            //redicrects to Mode list after running DeleteMessage
            header("location: ?mode=list");

            break;
        case "SAVE":
            $id = filter_input(INPUT_POST, "iMessageID", FILTER_SANITIZE_NUMBER_INT);


            if ($id > 0) {

                $obj = new Message();
                //Runs $obj->DeleteMessage on load
                $obj->UpdateMessage($id);
                //redicrects to Mode list after running DeleteMessage

            }
            if ($id == -1) {

                showme($_POST);
                $obj = new Message();
                //Runs $obj->DeleteMessage on load
                $obj->CreateMessage($id);
                //redicrects to Mode list after running DeleteMessage

            }

            header("location: ?mode=list");
            break;
    }
} else {
    header("location: /cms/Calendar.php");
}

