<?php
/**
 * Created by PhpStorm.
 * Auther @ Mads Roloff - Rights reservede to Author
 * Date: 17-05-2017
 */ ?>
<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/init.php";
$mode = setMode();
$strModuleName = "Brugere";
if ($auth->user->sysadmin || $auth->user->admin) {
    switch (strtoupper($mode)) {

        Default:
            sysBackendHeader();
            require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/nav.php";
            $frontpage = new frontpage();
            $frontpage->GetList();

            $iItemID = filter_input(INPUT_GET, "iItemID", FILTER_SANITIZE_NUMBER_INT);
            ?>
            <div class="container">
                <div class="row">

                    <div class="col-md-12">
                        <h4>Content Table</h4>
                        <div class="table-responsive">

                            <table class="table table-bordred table-striped">
                                <?php
                                $list = new listpresenter();
                                $list->ListMaker($frontpage->arrLabels, $frontpage->unset, $frontpage->arrValues, "iItemID", true, false);
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


            <?php


            sysBackendFooter();
            break;

        case "DETAILS":
            sysBackendHeader();
            require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/nav.php";
            $frontpage = new frontpage();

            $iItemID = filter_input(INPUT_GET, "iItemID", FILTER_SANITIZE_NUMBER_INT);

            $frontpage->getDetails($iItemID);
            ?>
            <div class="container">
                <div class="row">

                    <div class="col-md-12">
                        <h4>Content Details</h4>
                        <div class="table-responsive">


                            <table class="table table-bordred table-striped">
                                <?php
                                $list = new listpresenter();
                                $list->MakeDetails($frontpage->arrLabels, $frontpage->unset, $frontpage->arrValues);
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <?php
            sysBackendFooter();
            break;

        case "EDIT":
            sysBackendHeader();
            require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/nav.php";
            $frontpage = new frontpage();
            $fields = $frontpage->arrFormElm;
            $iItemID = filter_input(INPUT_GET, "iItemID", FILTER_SANITIZE_NUMBER_INT);

            $image = new image();
            $select = $image->GetSelect();

            ?>
            <div class="container">
                <div class="row">


                    <div class="col-md-12">


                        <?php
                        if ($iItemID == -1) {
                            ?>
                            <h4>Opret Content!</h4>
                            <?php
                        } else {
                            ?>
                            <h4>Rediger Content!</h4>
                            <?php
                        }
                        //get the item ID;
                        if ($iItemID == 0 || $iItemID == "-1") {
                        } else {
                            $frontpage->getfrontpage($iItemID);
                        }


                        $form = new FormPresenter();
                        $form->MakeForm($fields, $frontpage, $select, null, "frontpage", false, false, false);


                        ?>
                    </div>
                </div>
            </div>

            <?php
            sysBackendFooter();
            break;

        case
        "DELETE":
            $obj = new frontpage();
            $id = filter_input(INPUT_GET, "iItemID", FILTER_VALIDATE_INT);
            //Runs $obj->Deletefrontpage on load
            $obj->Deletefrontpage($id);
            //redicrects to Mode list after running Deletefrontpage
            header("location: ?mode=list");

            break;
        case "SAVE":
            $id = filter_input(INPUT_POST, "iItemID", FILTER_SANITIZE_NUMBER_INT);


            if ($id > 0) {

                $obj = new frontpage();
                //Runs $obj->Deletefrontpage on load
                $obj->Updatefrontpage($id);
                //redicrects to Mode list after running Deletefrontpage

            }
            if ($id == -1) {


                $obj = new frontpage();
                //Runs $obj->Deletefrontpage on load
                $obj->Createfrontpage($id);
                //redicrects to Mode list after running Deletefrontpage

            }

            header("location: ?mode=list");
            break;
    }
} else {
    header("location: /cms/Calendar.php");
}

