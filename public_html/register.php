<?php
/**
 * Created by PhpStorm.
 * Auther @ Mads Roloff - Rights reservede to Author
 * Date: 07-09-2017
 */
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/init.php";
sysHeader();
?>
    <div class="nav-bg section-space-small">
        <?php
        require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/nav.php";
        ?>
    </div>
<?php if (!$auth->checkSession()) { ?>
    <?php
    $mode = setMode();

    switch (strtoupper($mode)) {
        Default:
            ?>
            <article class="container">
                <section class="row">
                    <div class="col-lg-4 col-lg-offset-4">
                        <h2>Opret bruger!</h2>
                    </div>
                    <form method='POST' data-validate enctype='multipart/form-data'>
                        <!-- 2x hidden fields that gives id's and Modes to $_POST -->
                        <input name='mode' type='hidden' value='SAVE'>
                        <input data-requried="1" class='form-control' name='iUserID' type='hidden' placeholder='' value='-1'><br>

                        <div class="col-md-4 col-lg-offset-2">
                            <div class='form-group'>
                                <label>First Name</label></br><input data-requried="1" class='form-control' name='vcFirstName' type='text' placeholder='First name' value=''><br>
                            </div>
                            <div class='form-group'>
                                <label>Last Name</label></br><input data-requried="1" class='form-control' name='vcLastName' type='text' placeholder='Last name' value=''><br>
                            </div>
                            <div class='form-group'>
                                <label>user name</label></br><input data-requried="1" class='form-control' name='vcUserName' type='text' placeholder='Username' value=''><br>
                            </div>
                            <div class='form-group'>
                                <label>password</label>
                                </br><input data-validate="password" data-requried="1" class='form-control' id="password" name='vcPassword' type='password' placeholder='Password'><br>
                            </div>
                            <div class='form-group'>
                                <label>Confirm Password</label></br>
                                <input data-requried="1" data-validate="passwordConfirm" id="password_confirm" class='form-control' name='ConfirmPassword' type='password' placeholder='Confirm Password'><br>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class='form-group'>
                                <label>Adresse</label></br><input data-requried="1" class='form-control' name='vcAddress' type='text' placeholder='Address' value=''><br>
                            </div>
                            <div class='form-group'>
                                <label>Zip code</label></br><input data-requried="1" class='form-control' name='iZip' type='text' placeholder='Zip Code' value=''><br>
                            </div>
                            <div class='form-group'>
                                <label>City</label></br><input data-requried="1" class='form-control' name='vcCity' type='text' placeholder='City' value=''><br>
                            </div>
                            <div class='form-group'>
                                <label>Email</label></br><input data-requried="1" class='form-control' name='vcEmail' type='email' placeholder='Email' value=''><br>
                            </div>
                            <div class='form-group'>
                                <label>Phone Number</label></br><input data-requried="1" class='form-control' name='vcPhone1' type='tel' placeholder='Phone Number' value=''><br>
                            </div>

                        </div>
                        <div class="col-lg-4 col-lg-offset-4">
                            <button type='submit' name='submit' style="width:100%; margin-bottom: 5px;" class="btn btn-success">GEM</button>
                            <a href='login.php' class="btn btn-danger" style="width:100%;">Tilbage</a>
                        </div>
                    </form>
                </section>
            </article>
            <?php
            break;
        case "SAVE":
            $id = filter_input(INPUT_POST, "iUserID", FILTER_SANITIZE_NUMBER_INT);
            if ($id == -1) {

                showme($_POST);
                $obj = new user();
                //Runs $obj->DeleteUser on load
                $obj->CreateUser($id);
                //redicrects to Mode list after running DeleteUser
                header("location: login.php");
            }
            break;
    }
} else {
    header("location: index.php");
}

sysFooter();
?>