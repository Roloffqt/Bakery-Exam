<?php
/*
 * Author @ Mads Roloff - Roloff-design
 */

//display errors set to zero to disable;
ini_set("display_errors", 1);

//set timezone;
date_default_timezone_set("Europe/Copenhagen");

//define constants;
define("DOCROOT", filter_input(INPUT_SERVER, "DOCUMENT_ROOT", FILTER_SANITIZE_STRING)); // uno euro/
//define("DOCROOT", filter_input(INPUT_SERVER, "DOCUMENT_ROOT", FILTER_SANITIZE_STRING) . "/SvendeproeveMadsRoloff/public_html"); // school server and one.com

define("COREPATH", substr(DOCROOT, 0, strrpos(DOCROOT, "/")) . "/core/");

define("HOSTPATH", 0); // 1 IF UPLOAD TO SCHOOL SERVER OR ONE, 0 IF UPLOAD TO UNO EURO;


//EasyPHP
//define("DOCROOT", filter_input(INPUT_SERVER, "DOCUMENT_ROOT", FILTER_SANITIZE_STRING));
//define("COREPATH", substr(DOCROOT, 0, strrpos(DOCROOT, "/")) . "/core/");


//Functions
require_once($_SERVER["DOCUMENT_ROOT"] . '/../core/functions.php');
/*ClassLoader*/
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . '/../core/classes/ClassLoader.php';
$Classloader = new ClassLoader();
//$db = new db();
$db = new dbconf();
$setup = new Setup();

$imageSel = new image();

$auth = new auth();
$auth->iShowLoginForm = 0;
$auth->authentificate();