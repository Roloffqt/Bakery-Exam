<nav style="  z-index: +99;" id="navbar-primary" class="navbar hidden-xs text-center" role="navigation">
    <div style="padding: 0 !important;" class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-primary-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="navbar-primary-collapse">
            <ul class="nav navbar-nav">

                <li><a href="search.php" class="nav-font-size"><i class="fa fa-search" aria-hidden="true"></i></a></li>
                <li class="active"><a href="index.php" class="nav-font-size">Forside</a></li>
                <li><a href="product.php" class="nav-font-size">Produkter</a></li>
                <li><a href="index.php"><h1 class="logo" style="text-transform: Lowercase !important;">Bageriet</h1></a></li>
                <li><a href="contact.php" class="nav-font-size">Kontakt</a></li>
                <li><a href="blog.php" class="nav-font-size">Blog</a></li>
                <?php if (!$auth->checkSession()) { ?>
                    <li><a href="login.php" class="nav-font-size">Login</a></li>
                <?php } else { ?>
                    <li><a style="padding: 10px;margin: 5px;" href="?action=logout" class="nav-font-size right logout-btn btn "> Log Ud</a></li>
                <?php } ?>

            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<div class="row">
    <div style="  z-index: +99;" class="navbar navbar-default visible-xs">
        <div class="navbar-header">
            <span class="Position">Bageriet</span>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="index.php">Forside</a></li>
                <li><a href="#">Produkter</a></li>
                <li><a href="#">Kontakt</a></li>
                <li><a href="#">Blog</a></li>
                <?php if (!$auth->checkSession()) { ?>
                    <li><a href="login.php">Login</a></li>
                <?php } else { ?>
                    <li><a style="padding: 10px;margin: 5px;" href="?action=logout" class="right logout-btn btn"> Log Ud</a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>
