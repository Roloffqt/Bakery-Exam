<?php
/**
 * Created by PhpStorm.
 * Auther @ Mads Roloff - Rights reservede to Author
 * Date: 07-02-2017
 */ ?>
<footer>
    <section class="margin-null row">
        <div class="footer-margin text-center col-md-12 col-sm-12 col-xs-12 col-lg-4 col-lg-offset-4">

            <button id="return-to-top" type="button" class="btn btn-default btn-circle btn-xl"><i class="glyphicon glyphicon-menu-up"></i></button>
            <div class="section-space-small">
                <h2 class="logo">Bageriet</h2>
                <p>Lorem ipsum</p>
            </div>
        </div>
    </section>
</footer>


<!-- Scripts -->

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="https://use.fontawesome.com/45215596e7.js"></script>
<!-- Custom scripts -->
<script src="/assets/js/slick.min.js"></script>
<script src="/assets/js/functions.js"></script>
<script src="/assets/js/validate.js"></script>
<script type="text/javascript">
     $(document).on('ready', function () {
          $(".regular").slick({
               dots: false,
               infinite: true,
               slidesToShow: 5,
               slidesToScroll: 1,
               arrows: true
          });
     });
</script>
