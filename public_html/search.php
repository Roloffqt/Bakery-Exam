<?php
/**
 * Auther Mads Roloff
 * improved and hacked by Morten mandrup (ExeQue)
 * Code mostly by Morten Mandrup (ExeQue)
 */


require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/init.php";
sysHeader();
?>
    <div class="nav-bg">
        <?php
        require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/nav.php";
        ?>
    </div>


<?php
//Finds button with name Search-serach
if (isset($_POST['search'])) {
    //takes whatever you type in input and makes it lovercase
    $search = mb_strtolower($_POST['search']);

    // --- Fix by Morten Harders -->

    // remove all non-alphanumeric characters except spaces and dashes
    $search = preg_replace('/[^a-z0-9\-\sæøåÆØÅ]|\-\-+/', "", $search);

    // split the string into multiple terms
    $terms = explode(" ", $search);
    //Searches your "keywords" in database IN LOWER CASE FORMAT
    $clauses = array();
    if (isset($_POST["category"]) && $_POST["category"] == "shopproduct") {
        $stmt = "SELECT DISTINCT  * FROM shopproduct WHERE ";
        foreach ($terms as $term) {
            $clauses[] = "LOWER(vcTitle) LIKE '%$term%'";
        }
    } else {
        $stmt = "SELECT DISTINCT sp.* FROM shopproduct sp
                          INNER JOIN prodingrederel prel ON sp.iProductID = prel.iProductID
                          INNER JOIN ingredeints ing ON ing.iIngredeintID = prel.iIngredeintID WHERE ";
        foreach ($terms as $term) {
            $clauses[] = "LOWER(ing.vcTitle) LIKE '%$term%'";
        }
    }
    $stmt .= implode(" OR ", $clauses);

    $results = $db->_fetch_array($stmt);

    // <-- Fix by Morten Harders --
    ?>
    <article class="container">
        <section clasS="row">
            <h1>Search page</h1>
            <!-- Form action = file you want to be executed and method = POST -->
            <div class="col-lg-12">
                <form style="width: 100%;" method="POST" action="search.php" class="input-item">
                    <div>
                        <div class="" style="width:100%;">
                            <select name="category">
                                <option value="shopproduct">Produtker</option>
                                <option value="ingredeints">ingredeints</option>
                            </select>
                            <input style=";" type="text" name="search" placeholder="Search">
                            <button style="    padding: 14px 75px;" class="" type="submit">Søg</button>
                        </div>
                    </div>
                </form>
            </div>
            <div style="margin-top: 100px;" class="col-lg-12">
                <div class="product-container">
                    <?php
                    //does awesome db stuff
                    if ($results > 0) foreach
                    ($results

                    as $key => $row) {

                    ?>
                    <!-- Display results with information from the database -->

                    <div class="col-sm-4 col-lg-3 col-md-4 id="<?php echo "product" . $row["iProductID"] ?>">

                    <img style="height: 100%; width: 100%;" src="<?php echo $row["vcImage"] ?>" alt="">
                    <div class="caption text-center">
                        <h4>
                            <a href="<?php echo "product.php?mode=Details&iProductID=" . $row["iProductID"] ?>"><?php echo $row["vcTitle"] ?></a>
                        </h4>
                        <p><?php echo substr($row["txDesc"], 0, 100); ?></p>

                    </div>
                </div>
                <?php }
                ?>
            </div>
            </div>

        </section>

    </article>

    <?php
}
sysFooter();
