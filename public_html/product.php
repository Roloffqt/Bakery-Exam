<?php
/**
 * Created by PhpStorm.
 * Auther @ Mads Roloff - Rights reservede to Author
 * Date: 26-09-2017
 */

/**
 * If else statement by Glen Jensen
 */
if (is_file(filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/init.php")) {
    require_once(filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/init.php");
} else {
    require_once(filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/public_html/assets/incl/init.php");
}

sysHeader();
?>
    <div class=" nav-bg section-space-med">
        <?php
        require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/nav.php";
        ?>
    </div>

<?php
$mode = setMode();

switch (strtoupper($mode)) {

    default:
        $iCategoryID = Filter_input(INPUT_GET, "iCategoryID", FILTER_SANITIZE_NUMBER_INT);

        $product = new shopproduct();
        $prod = $product->getAllLimit(100);
        $shopcategory = new shopcategory();
        $row = $shopcategory->GetSelect();
        if (isset($iCategoryID)) {

        }

        ?>
        <article class="container">
            <section class="row section-space">
                <div class=" col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12 text-center section-space">
                    <?php if ($iCategoryID == null) { ?>
                        <h2 class="logo-black">Nyste bagværk</h2>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                            when an unknown printer took
                            a galley of type and scrambled</p>
                    <?php } else {
                        $cateheading = $shopcategory->GetItem($iCategoryID); ?>
                        <h2 class="logo-black">Nyste bagværk</h2>
                        <p><?php echo $cateheading[0]["txDesc"] ?></p>

                    <?php } ?>
                </div>
                <div class="col-lg-offset-2 col-lg-1">
                    <div class="col-lg-12 text-left">
                        <?php
                        foreach ($row as $value) {
                            //Definer array til CSS klasser for hver celle
                            $arrClasses = array(
                                1 => "prodcategory",
                            );
                            if ($value["iCategoryID"] == isset($_GET["iCategoryID"])) {
                                $arrClasses[] = "highlight";
                            }
                            echo '<li class="li-style"><a class="' . implode($arrClasses, " ") . '" href="product.php?iCategoryID=' . $value["iCategoryID"] . '">' . $value["vcTitle"] . '</a></li>';

                        }
                        ?>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-12 col-md-12 col-xs-12">
                    <?php

                    if (isset($_GET["iCategoryID"]) > 0) {
                        $prod = $product->getCategory($_GET["iCategoryID"]);
                        //Make sql for getting icategoryid only
                        foreach ($prod as $key => $row): ?>

                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <div class="card text-center">
                                    <div clasS="col-lg-12 col-md-12">
                                        <img style="max-width: 100%; margin-bottom: 50px; border-radius:180px;" src="<?php echo $row["vcImage"] ?>" alt="Image">
                                    </div>
                                    <h5 style=""><strong><?php echo $row["vcTitle"] ?></strong></h5>

                                    <p><?php echo substr($row["txDesc"], 0, max(40, $row["txDesc"])) ?></p>

                                    <a href="product.php?mode=Details&iProductID=<?php echo $row["iProductID"] ?>" class="btn btn-default product-seemore">se mere</a>
                                </div>
                            </div>
                            <?php
                        endforeach;
                    } else {
                        foreach ($prod as $key => $row):
                            $rows = $db->_fetch_value("SELECT COUNT(iLike) FROM productlike WHERE iProductID = " . $row["iProductID"]);
                            $comment = new comment();
                            $count = $comment->commentCount($row["iProductID"]);

                            ?>
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <div class="card text-center">
                                    <div clasS="col-lg-12 col-md-12">
                                        <img style="max-width: 100%; margin-bottom: 50px; border-radius:180px;" src="<?php echo $row["vcImage"] ?>" alt="Image">
                                        <p class="comment-count"><?php echo $count ?><i class="fa fa-comment" aria-hidden="true"></i></p>
                                        <a class="font-color-grey comment-amount" href="product.php?mode=Details&iProductID=<?php echo $row["iProductID"] ?>"><?php echo $rows ?>
                                            <i class="fa fa-thumbs-up" aria-hidden="true"></i></a>

                                    </div>
                                    <h5 style=""><strong><?php echo $row["vcTitle"] ?></strong></h5>

                                    <p><?php echo substr($row["txDesc"], 0, max(40, $row["txDesc"])) ?></p>

                                    <a href="product.php?mode=Details&iProductID=<?php echo $row["iProductID"] ?>" class="btn btn-default no-border-radius product-seemore">se mere</a>

                                </div>
                            </div>
                        <?php endforeach;
                    } ?>
                </div>
            </section>
        </article>
        <?php
        break;

    case "DETAILS":
        $iProductID = Filter_input(INPUT_GET, "iProductID", FILTER_SANITIZE_NUMBER_INT);


        $product = new shopproduct();
        $product->getItem($iProductID);


        //Finds the category for current Product
        $sql = "SELECT * FROM shopcatprodrel INNER JOIN shopproduct ON shopcatprodrel.iProductID  = shopproduct.iProductID WHERE shopproduct.iProductID = " . $iProductID;
        $getcatname = $db->_fetch_array($sql);
        $catselect = "SELECT * FROM shopcategory WHERE iCategoryID = " . $getcatname[0]["iCategoryID"];
        $catename = $db->_fetch_array($catselect);

        ?>
        <article class="container">
            <section class="row">
                <?php
                require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/SearchField.php";
                ?>
                <div class="col-lg-9 section-space-med margin-remove-top">
                    <h2><?php echo $product->vcTitle ?></h2>
                    <h4><?php echo $catename[0]["vcTitle"] ?></h4>
                </div>
                <?php if ($auth->checkSession()) { ?>
                    <div class="col-lg-3">
                        <a style="margin-top: 20px; margin-bottom: 10px;" href="assets/scripts/like.php?iProductID=<?php echo $iProductID ?>" class="like-btn btn btn-default">Like!
                            <i class="font-color-red fa fa-heart-o" aria-hidden="true"></i></a>
                    </div>
                <?php } ?>
                <div class="col-lg-8">
                    <img style="float:left; height:150px;" src="<?php echo $product->vcImage ?>" alt="<?php echo $product->vcTitle ?>">
                    <p class="text-left"> <?php echo $product->txDesc ?></p>
                </div>
                <div class="col-lg-4 col-xs-12 section-space-med">
                    <?php
                    $ing = new ingredeint();
                    //$row = $ing->GetSelect();
                    $row = $ing->getAmount($iProductID);


                    ?>
                    <h3 class="text-left margin-top-null " style="margin-bottom: 20px; margin-top:0">Ingredienser</h3>
                    <?php foreach ($row as $value): ?>

                        <h4 class="text-center ing-box"><?php echo $value["iAmount"] ?>. <?php echo $value["vcTitle"] ?></h4>


                    <?php endforeach; ?>
                </div>
            </section>
        </article>
        <?php
        $comment = new comment();
        $count = $comment->commentCount($iProductID);

        ?>


        <article class="container">
            <section class="row">

                <div style="padding: 0 !important;">
                    <div class="bg-white comment-box col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-xs-4">
                            <h4>Kommentarer</h4>
                        </div>
                        <div class="text-right col-xs-8">

                            <p class="comment-count"><?php echo $count ?><i class="fa fa-comment" aria-hidden="true"></i></p>
                        </div>

                    </div>
                </div>
                <?php if ($auth->checkSession()) { ?>
                    <div style=" padding: 0 !important;" class="section-space-big hidden-sm hidden-md hidden-xs col-lg-12 ">
                        <form style="left: 50.5%;" data-validate class=" input-item comment-style" method="POST" action="<?php echo "/assets/scripts/comment.php" ?>">
                            <input type="hidden" name="iProductID" value="<?php echo $row[0]["iProductID"] ?>"/>
                            <input type="hidden" name="iNewsID" value="<?php echo 0 ?>"/>
                            <input type="hidden" name="iUserID" value="<?php echo $auth->iUserID ?>"/>
                            <input type="hidden" name="vcImage" value="<?php echo $auth->user->vcImage ?>"/>
                            <input type="hidden" name="vcTitle" value="<?php echo $auth->user->vcUserName ?>"/>
                            <label for="txContent"><i class="fa fa-pencil" aria-hidden="true"></i></label>
                            <button>post</button>
                            <div class="input-ctn">
                                <input data-requried="1" type="text" name="txContent" id="txContent" placeholder="fortæl os hvad du synes..."/>
                            </div>

                        </form>
                    </div>

                    <?php
                }

                /**
                 *
                 *  Auther : Heinz HEKA
                 *  Tech college aalborg
                 *  Modifyed by Mads Roloff
                 */
                $page = filter_input(INPUT_GET, "page", FILTER_SANITIZE_NUMBER_INT,
                    array("options" => array("default" => 1)));
                $limit = 3;
                $row = $comment->listComments($iProductID, $limit);

                $start = ($page - 1) * $limit;

                $params = array($start, $limit);
                $sql = "SELECT * FROM comment INNER JOIN user ON comment.iUserID = user.iUserID WHERE iProductID = $iProductID LIMIT ?, ?";
                $rows = $db->_fetch_array($sql, $params);

                $num_records = $db->_fetch_value("SELECT COUNT(*) FROM comment WHERE iProductID = " . $iProductID);
                $num_pages = ceil($num_records / $limit);


                foreach ($rows as $value):

                    ?>
                    <div class="bg-white comment-box col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                            <img style="width: 100px; height: 100px; max-height:100% !important;" class="img-circle" src="<?php echo $value["vcImage"] ?>" alt="<?php echo $value["vcTitle"] ?>">
                        </div>
                        <div class="col-lg-10 col-md-9 col-sm-8 col-xs-12">
                            <div style="display: inline-block">
                                <h3><?php echo $value["vcTitle"] ?></h3>
                                <p><?php echo date("j F, Y, g:i a", $value["DaCreated"]) ?></p>
                                <p><?php echo $value["txContent"] ?></p>
                            </div>
                        </div>
                    </div>
                    <?php

                endforeach;
                ?>

            </section>

        </article>
        <article class="container">


            <section class="row section-space-small">
                <ul class="pagination no-border-radius">

                    <?php $prevPage = filter_input(INPUT_GET, "page", FILTER_SANITIZE_NUMBER_INT); ?>
                    <li><a href="?mode=Details&iProductID=<?php echo $iProductID ?>&page=<?php ($prevPage > 1 ? $prevPage-- : $prevPage);
                        echo $prevPage; ?>">&laquo;</a></li>

                    <?php
                    for ($i = 1; $i <= $num_pages; $i++) {
                        echo "<li><a class='no-border-radius page_numbers' href=\"?mode=Details&iProductID=" . $iProductID . "&page=" . $i . "\">" . $i . "</a></li>&nbsp;\n";
                    }
                    $nextPage = filter_input(INPUT_GET, "page", FILTER_SANITIZE_NUMBER_INT);
                    ?>
                    <li><a href="?mode=Details&iProductID=<?php echo $iProductID ?>&page=<?php ($nextPage < $num_pages ? $nextPage++ : $nextPage);
                        echo $nextPage; ?>">&raquo;</a></li>


                </ul>
            </section>
        </article>
        <?php
        break;

}
sysFooter();
?>