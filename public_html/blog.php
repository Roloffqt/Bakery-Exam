<?php
/**
 * Created by PhpStorm.
 * Auther @ Mads Roloff - Rights reservede to Author
 * Date: 28-09-2017
 */
if (is_file(filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/init.php")) {
    require_once(filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/init.php");
} else {
    require_once(filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/public_html/assets/incl/init.php");
}
$contact = new contact();

$row = $contact->GetSelect();

sysHeader();
?>
<div class="nav-bg">
    <?php
    require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/nav.php";
    //require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/SearchField.php";
    ?>
</div>
<?php

$mode = setMode();

switch (strtoupper($mode)) {
    default:
        ?>
        <article class="container">
            <section class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h2 clasS="text-center logo logo-black">Blog</h2>
                </div>
            </section>

            <section class="row section-space">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-offset-2 col-lg-8">
                        <?php
                        $news = new news();
                        $row = $news->GetSelect(16);

                        foreach ($row

                                 as $value) {

                            ?>
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <a href="?mode=details&iNewsID=<?php echo $value["iNewsID"] ?>">
                                    <div class="card text-center">
                                        <div clasS="col-lg-12 col-md-12">
                                            <img class="" style="max-width: 100%; margin-bottom: 50px; border-radius:180px;" src="<?php echo $value["vcImage"] ?>" alt="Image">

                                        </div>
                                        <h5 class="font-color-grey" style=""><strong><?php echo $value["vcTitle"] ?></strong></h5>

                                        <p class="font-color-grey"><?php echo substr($value["txShortDesc"], 1, max(150, $value["txShortDesc"])) ?></p>
                                    </div>
                                </a>

                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </section>
        </article>

        <?php
        break;
    case "DETAILS":
        $iNewsID = Filter_input(INPUT_GET, "iNewsID", FILTER_SANITIZE_NUMBER_INT);


        $news = new news();
        $row = $news->GetItem($iNewsID);

        //Finds the category for current Product
        //    $sql = "SELECT * FROM shopcatprodrel INNER JOIN shopproduct ON shopcatprodrel.iProductID  = shopproduct.iProductID WHERE shopproduct.iProductID = " . $iProductID;
        //  $getcatname = $db->_fetch_array($sql);
        //  $catselect = "SELECT * FROM shopcategory WHERE iCategoryID = " . $getcatname[0]["iCategoryID"];
        //   $catename = $db->_fetch_array($catselect);

        ?>
        <article class="container">
            <section class="row">
                <?php
                require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/SearchField.php";
                ?>
                <div class="col-lg-9 section-space-med margin-remove-top">
                    <h2><?php echo $row[0]["vcTitle"] ?></h2>

                </div>
                <?php if ($auth->checkSession()) { ?>
                    <div class="col-lg-3">
                        <a style="margin-top: 20px; margin-bottom: 10px;" href="assets/scripts/like.php?iProductID=<?php echo $row[0]["iNewsID"] ?>" class="like-btn btn btn-default">Like!
                            <i class="font-color-red fa fa-heart-o" aria-hidden="true"></i></a>
                    </div>
                <?php } ?>
                <div class="col-lg-8 col-lg-offset-2">
                    <img class="img-circle" style="float:left; margin: 20px 20px 0 0; height:150px;" src="<?php echo $row[0]["vcImage"] ?>" alt="<?php echo $row[0]["vcTitle"] ?>">
                    <p class="text-left"> <?php echo $row[0]["txDesc"] ?></p>
                </div>

            </section>
        </article>
        <?php
        $comment = new comment();
        $count = $comment->commentCount($iNewsID);

        ?>


        <article class="container">
            <section class="row">

                <div style="padding: 0 !important;">
                    <div class="bg-white comment-box col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-xs-4">
                            <h4>Kommentarer</h4>
                        </div>
                        <div class="text-right col-xs-8">

                            <p class="comment-count"><?php echo $count ?><i class="fa fa-comment" aria-hidden="true"></i></p>
                        </div>

                    </div>
                </div>
                <?php if ($auth->checkSession()) { ?>
                    <div class="padding-reset section-space-med hidden-sm hidden-md hidden-xs col-lg-12 ">
                        <form data-validate style="left: 50.5%;" class=" input-item comment-style" method="POST" action="<?php echo "/assets/scripts/comment.php" ?>">
                            <input type="hidden" name="iProductID" value="<?php echo 0 ?>"/>
                            <input type="hidden" name="iNewsID" value="<?php echo $row[0]["iNewsID"] ?>"/>
                            <input type="hidden" name="iUserID" value="<?php echo $auth->iUserID ?>"/>
                            <input type="hidden" name="vcImage" value="<?php echo $auth->user->vcImage ?>"/>
                            <input type="hidden" name="vcTitle" value="<?php echo $auth->user->vcUserName ?>"/>
                            <label for="txContent"><i class="fa fa-pencil" aria-hidden="true"></i></label>
                            <button>post</button>
                            <div class="input-ctn">
                                <input data-requried="1" type="text" name="txContent" id="txContent" placeholder="fortæl os hvad du synes..."/>
                            </div>

                        </form>
                    </div>

                    <?php
                }

                /**
                 *
                 *  Auther : Heinz HEKA
                 *  Tech college aalborg
                 *  Modifyed by Mads Roloff
                 */
                $page = filter_input(INPUT_GET, "page", FILTER_SANITIZE_NUMBER_INT,
                    array("options" => array("default" => 1)));
                $limit = 3;
                $row = $comment->listComments($iNewsID, $limit);

                $start = ($page - 1) * $limit;

                $params = array($start, $limit);
                $sql = "SELECT * FROM comment WHERE iNewsID = $iNewsID LIMIT ?, ?";
                $rows = $db->_fetch_array($sql, $params);

                $num_records = $db->_fetch_value("SELECT COUNT(*) FROM comment WHERE iNewsID = " . $iNewsID);
                $num_pages = ceil($num_records / $limit);


                foreach ($rows as $value):

                    ?>
                    <div class="bg-white comment-box col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                            <img style="width: 100px; height: 100px; max-height:100% !important;" class="img-circle" src="<?php echo $value["vcImage"] ?>" alt="<?php echo $value["vcTitle"] ?>">
                        </div>
                        <div class="col-lg-10 col-md-9 col-sm-8 col-xs-12">
                            <div style="display: inline-block">
                                <h3><?php echo $value["vcTitle"] ?></h3>
                                <p><?php echo date("j F, Y, g:i a", $value["DaCreated"]) ?></p>
                                <p><?php echo $value["txContent"] ?></p>
                            </div>
                        </div>
                    </div>
                    <?php

                endforeach;
                ?>

            </section>

        </article>
        <article class="container">

            <section class="row section-space-small">
                <ul class="pagination no-border-radius">
                    <?php $prevPage = filter_input(INPUT_GET, "page", FILTER_SANITIZE_NUMBER_INT); ?>
                    <li><a href="?mode=Details&iNewsID=<?php echo $iNewsID ?>&page=<?php ($prevPage > 1 ? $prevPage-- : $prevPage);
                        echo $prevPage; ?>">&laquo;</a></li>
                    <?php
                    for ($i = 1; $i <= $num_pages; $i++) {
                        echo "<li><a class='no-border-radius page_numbers' href=\"?mode=Details&iNewsID=" . $iNewsID . "&page=" . $i . "\">" . $i . "</a></li>&nbsp;\n";
                    }

                    $nextPage = filter_input(INPUT_GET, "page", FILTER_SANITIZE_NUMBER_INT);
                    ?>
                    <li><a href="?mode=Details&iNewsID=<?php echo $iNewsID ?>&page=<?php ($nextPage < $num_pages ? $nextPage++ : $nextPage);
                        echo $nextPage; ?>">&raquo;</a></li>
                </ul>
            </section>
        </article>
        <?php
        break;
}
sysFooter();
?>
